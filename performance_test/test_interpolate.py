#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import sys
import shutil
import numpy as np

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import alyatab
from alyatab.table import AlyaTable


#############
# Get table #
#############
tab = AlyaTable()  # typ="single")
orig_tab_path = os.path.join(
    "..",
    "tests",
    "data",
    "example_flamelet_table",
    "example_flamelet_table.dat",
)

#
# Read table
#
tab.readText(orig_tab_path)


#
# Test 2D
#
valarr = np.zeros([100, 100, 2])
valarr[0, 0, :] = [0.05, 0.75]
valarr[1, 0, :] = [0.0, 0.0]
valarr[2, 0, :] = [1.0, 1.0]
valarr[0, 1, :] = [0.052, 0.78]
valarr[1, 1, :] = [0.02, 0.02]
valarr[2, 1, :] = [0.98, 0.98]

goaltempe = np.zeros([3, 2])
goaltempe[0, 0] = 1622.8301921
goaltempe[1, 0] = 298.1499938964844
goaltempe[2, 0] = 298.1500549316406
goaltempe[0, 1] = 1707.25997803
goaltempe[1, 1] = 315.4725606
goaltempe[2, 1] = 320.29501719

data = tab.interpolateArray(valarr, key_list=["Z", "C", "T"], verbose=True)

assert np.all(np.abs(data["Z"] - valarr[:, :, 0]) < 1e-6)
assert np.all(np.abs(data["C"] - valarr[:, :, 1]) < 1e-6)
assert np.all(np.abs(data["T"][:3, :2] - goaltempe) < 1e-4)
