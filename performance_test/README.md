## Profiling with cProfile

Running with profiler module on:
python3 -m cProfile -o profile.dat script.py

Investigat profile with pstats module:
python3 -m pstats profile.dat

-help
-stats <n>:
prints firs n statistics
too messy -> use strip
-strip:
short path
-sort <key>
time: time spent in function
cumtime: time spent within the function and in the children of the function
without arg: list of options
