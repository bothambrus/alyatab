# AlyaTab

Table class writing input files for [Alya](https://gitlab.com/bsc-alya/alya) mainly for tabulated chemistry methods ([FlameGen](https://gitlab.com/bothambrus/flamegen)).

For details on usage, see the [wiki](https://gitlab.com/bothambrus/alyatab/-/wikis/home).

