init:
	pip3 install -r requirements.txt

checkstyle:
	black --check alyatab 
	black --check tests
	black --check examples
	black --check plot
	isort --profile black --diff alyatab
	isort --profile black --diff tests
	isort --profile black --diff examples

style:
	isort --profile black alyatab
	black alyatab
	black tests
	black examples
	black plot
	black performance_test

test:
	rm -rf tests/tmp_testing/
	pytest --junitxml=junit_report.xml --cov=alyatab --cov-report=html tests
	coverage xml
	mkdir -p dist 
	rm -rf dist/htmlcov
	mv coverage.xml dist/
	mv htmlcov dist/
	echo open dist/htmlcov/index.html to review code coverage
	python .cicd_scripts/test_coverage.py 90 dist/coverage.xml



.PHONY: init test

