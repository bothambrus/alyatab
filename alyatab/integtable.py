#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import sys

import numpy as np

from alyatab.difftable import DifferentiatingAlyaTable


class IntegratingAlyaTable(DifferentiatingAlyaTable):
    """
    Class to hold tabulated data for Alya with numerical integration capabilities.
    """

    def __init__(
        self, dic={}, keys=[], coordKeys=[], coords={}, typ="single", overwrite=True
    ):
        #
        # Initialize parent class
        #
        DifferentiatingAlyaTable.__init__(
            self,
            dic=dic,
            keys=keys,
            coordKeys=coordKeys,
            coords=coords,
            typ=typ,
            overwrite=overwrite,
        )

    def weightsOfPartialIntegrationAlongOneCoordinate(
        self, ck, weightkey, coeffkey=None, scheme="trapezoidal"
    ):
        """
        Calculate weights at each data point, such, that `np.sum( self.data[weightkey], axis=ick)` corresponds to integration along the `ck` coordinate.
        The weights may be pre-multiplied by a coefficient: `self.data[coeffkey]`
        """
        #
        # Exit if irrelevant input is given
        #
        if not ck in self.coordKeys:
            print("Key {} is not among table coordinates for integration.".format(ck))
            sys.stdout.flush()
            return

        #
        # Add grid of coordinate of differentiation
        #
        self.addGridOfCoordinate(ck, overwrite=False)
        self.addEmptyData("tmpIntegrationWeight")
        self.data["tmpIntegrationWeight"] = np.zeros_like(
            self.data["tmpIntegrationWeight"], dtype=self.dtype
        )

        #
        # Index of integrated coordinate
        #
        ick = self.coordKeys.index(ck)

        #
        # Get diff of coordinate
        #
        dx = np.diff(self.data[ck], axis=ick)

        if scheme == "forward":
            self.setSlice(
                "tmpIntegrationWeight",
                {
                    ck: {
                        "type": "indlist",
                        "indlist": range(0, len(self.coords[ck]) - 1),
                    }
                },
                dx,
            )
        elif scheme == "backward":
            self.setSlice(
                "tmpIntegrationWeight",
                {
                    ck: {
                        "type": "indlist",
                        "indlist": range(1, len(self.coords[ck])),
                    }
                },
                dx,
            )
        elif scheme == "trapezoidal":
            #
            # Forward half
            #
            self.setSlice(
                "tmpIntegrationWeight",
                {
                    ck: {
                        "type": "indlist",
                        "indlist": range(0, len(self.coords[ck]) - 1),
                    }
                },
                0.5 * dx,
            )
            weightPart, shapeND = self.getSlice(
                "tmpIntegrationWeight",
                {
                    ck: {
                        "type": "indlist",
                        "indlist": range(1, len(self.coords[ck])),
                    }
                },
            )
            #
            # Backward half
            #
            self.setSlice(
                "tmpIntegrationWeight",
                {
                    ck: {
                        "type": "indlist",
                        "indlist": range(1, len(self.coords[ck])),
                    }
                },
                weightPart + 0.5 * dx,
            )

        #
        # Set final field
        #
        self.addEmptyData(weightkey, overwrite=False)
        if not coeffkey is None:
            self.data[weightkey] = copy.deepcopy(self.data[coeffkey])
        else:
            self.data[weightkey] = np.ones(self.shape, dtype=self.dtype)
        self.data[weightkey] = self.data[weightkey] * self.data["tmpIntegrationWeight"]

        #
        # Delete temporary data
        #
        self.removeData("tmpIntegrationWeight")

    @classmethod
    def partialIntegrationAlongOneCoordinate(cls, inst, ck, key, scheme="trapezoidal"):
        """
        Integrate field `key` of `inst.data` along the `ck` coordinate, return the integrated table.
        """
        #
        # Exit if irrelevant input is given
        #
        if not ck in inst.coordKeys:
            print("Key {} is not among table coordinates for integration.".format(key))
            sys.stdout.flush()
            return
        if not key in inst.keys():
            print("Key {} is not among table dat for integration.".format(key))
            sys.stdout.flush()
            return

        #
        # Assemble weights for integration
        #
        inst.weightsOfPartialIntegrationAlongOneCoordinate(
            ck, "tmpWeight{}".format(ck), scheme=scheme
        )

        #
        # Index of integrated coordinate
        #
        ick = inst.coordKeys.index(ck)

        #
        # Determine data type
        #
        if inst.dtype == np.float32:
            typ = "single"
        elif inst.dtype == np.float64:
            typ = "double"

        #
        # Create table to hold integrated data
        #
        tab = cls(typ=typ)

        #
        # Add coordinates of new shape
        #
        for k in inst.coordKeys:
            if k != ck:
                tab.addCoordinate(k, inst.coords[k])

        #
        # Add integrated data
        #
        tab.addEmptyData(key)

        #
        # Integrate
        #
        tab.data[key] = np.sum(
            inst.data["tmpWeight{}".format(ck)] * inst.data[key], axis=ick
        )

        #
        # Delete temporary data
        #
        inst.removeData("tmpWeight{}".format(ck))

        return tab

    @classmethod
    def partialIntegration(cls, inst, ck_list, key, scheme="trapezoidal"):
        """
        Integrate field `key` of `self.data` along the multiple coordinates, and save result in new table.
        """
        #
        # Exit if irrelevant input is given
        #
        for ck in ck_list:
            if not ck in inst.coordKeys:
                print(
                    "Key {} is not among table coordinates for integration.".format(ck)
                )
                sys.stdout.flush()
                return
        if not key in inst.keys():
            print("Key {} is not among table dat for integrationa.".format(key))
            sys.stdout.flush()
            return

        #
        # Integration key and list of axes
        #
        intkey = "temporaryWeight"
        ick_list = []
        for ck in ck_list:
            intkey += "_{}".format(ck)
            ick_list.append(inst.coordKeys.index(ck))

        #
        # Initialize integration weights
        #
        inst.addEmptyData(intkey, overwrite=False)
        inst.data[intkey] = np.ones(inst.shape, dtype=inst.dtype)

        #
        # Repeated weight calculation
        #
        for ck in ck_list:
            inst.weightsOfPartialIntegrationAlongOneCoordinate(
                ck, weightkey=intkey, coeffkey=intkey, scheme=scheme
            )

        #
        # Determine data type
        #
        if inst.dtype == np.float32:
            typ = "single"
        elif inst.dtype == np.float64:
            typ = "double"

        #
        # Create table to hold integrated data
        #
        tab = cls(typ=typ)

        #
        # Add coordinates of new shape
        #
        for k in inst.coordKeys:
            if not k in ck_list:
                tab.addCoordinate(k, inst.coords[k])

        #
        # Add dummy coordinate if integration is ecacuted along all coordinates
        #
        if tab.dims == 0:
            tab.addCoordinate("placeholder", [0])

        #
        # Add integrated data
        #
        tab.addEmptyData(key)

        #
        # Integrate
        #
        tab.data[key] = np.sum(inst.data[intkey] * inst.data[key], axis=tuple(ick_list))
        if tab.__isFloat__(tab.data[key]):
            #
            # Format to array, of integration is ececuted along all coordinates
            # So the table data stays consistently the same.
            #
            tab.data[key] = np.array([tab.data[key]], dtype=tab.dtype)

        #
        # Delete temporary data
        #
        inst.removeData(intkey)

        return tab
