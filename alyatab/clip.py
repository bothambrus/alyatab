#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np


def clipUnderAndOverflow(x, doIt=True, snapToZero=True):
    """
    Clip float or numpy array to well representable floating point values.
    """
    if doIt:
        if snapToZero:
            snapToVal = 0.0
        else:
            snapToVal = 1e-16
        if isinstance(x, (float, int)):
            #
            # For floats
            #
            if abs(x) < 1e-16:
                if x == 0:
                    x = snapToVal
                else:
                    x = snapToVal * np.sign(x)
            elif x > 1e16:
                x = 1e16
            elif x < -1e16:
                x = -1e16
        elif isinstance(x, (list, np.ndarray)):
            #
            # For numpy arrays
            #
            x = np.array(x)
            mysign = np.sign(x)
            mysign = np.where(mysign == 0, 1.0, mysign)
            x = np.where(
                np.abs(x) < 1e-16,
                snapToVal * mysign,
                np.where(x > 1e16, 1e16, np.where(x < -1e16, -1e16, x)),
            )
    return x
