#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import sys

import numpy as np

from alyatab.basetable import BaseAlyaTable
from alyatab.index import inds2i


class SliceAlyaTable(BaseAlyaTable):
    """
    Class to hold tabulated data for Alya with slicing capabilities.
    """

    def __init__(
        self, dic={}, keys=[], coordKeys=[], coords={}, typ="single", overwrite=True
    ):
        #
        # Initialize parent class
        #
        BaseAlyaTable.__init__(
            self,
            dic=dic,
            keys=keys,
            coordKeys=coordKeys,
            coords=coords,
            typ=typ,
            overwrite=overwrite,
        )

    def getSliceIndeces(self, coord_treatment, verbose=False):
        """
        Get a slice indeces containing points from the full table.
        `coord_treatment` is a dictionary that specifies the treatment of specific coordinates by dictionaries:
            {"type": "ind", "ind": ind}:    only specific index
            {"type": "indlist", "indlist": array}:    only list of specific indeces
            {"type": "all"} or not present: do not limit this coordinate
        """
        #
        # Keys corresponding to slice
        #
        sliceCoordKeys = copy.deepcopy(self.coordKeys)
        for ck in self.coordKeys:
            if (ck in coord_treatment) and (coord_treatment[ck]["type"] == "ind"):
                #
                # If a single index is requested, it does not show up as a dimension
                #
                del sliceCoordKeys[sliceCoordKeys.index(ck)]

        #
        # Treatment
        #
        treatmentType = []
        for ck in self.coordKeys:
            if ck in coord_treatment:
                treatmentType.append(coord_treatment[ck]["type"])
            else:
                #
                # Default is "all"
                #
                treatmentType.append("all")

        #
        # Slice treatment
        #
        sliceTreatmentType = []
        for ii, ck in enumerate(sliceCoordKeys):
            sliceTreatmentType.append(treatmentType[self.coordKeys.index(ck)])

        #
        # Get shape of slice
        #
        sliceShape = np.empty([len(sliceCoordKeys)], dtype=int)
        for ii, ck in enumerate(sliceCoordKeys):
            #
            # Special treatment of each coordinate lenght
            #
            if sliceTreatmentType[ii] == "indlist":
                sliceShape[ii] = len(coord_treatment[ck]["indlist"])
            else:
                #
                # Default is "all"
                #
                sliceShape[ii] = len(self.coords[ck])

        #
        # Full shape corresponding to all dimensions of table
        #
        sliceFullShape = np.empty([len(self.coordKeys)], dtype=int)
        jj = 0
        for ii, ck in enumerate(self.coordKeys):
            if treatmentType[ii] == "ind":
                sliceFullShape[ii] = 1
            else:
                sliceFullShape[ii] = sliceShape[jj]
                jj += 1

        #################
        # Special cases #
        #################
        #
        # The requested data is not a real slice, but a full table, then it's easy
        #
        if all([tt == "all" for tt in treatmentType]):
            return np.array(range(np.prod(sliceShape))), sliceShape

        ################
        # General case #
        ################
        #
        # Full indeces
        #
        ind_full = []
        for ii, ck in enumerate(self.coordKeys):
            ind_full.append(None)
            #
            # Treatment of self
            #
            if treatmentType[ii] == "all":
                loc_ind = np.array(range(self.shape[ii]), dtype=int)
            elif treatmentType[ii] == "ind":
                loc_ind = np.array([coord_treatment[ck]["ind"]], dtype=int)
            elif treatmentType[ii] == "indlist":
                loc_ind = np.array(coord_treatment[ck]["indlist"], dtype=int)

            #
            # All the indeces of this coordinate
            #
            for jj, ck2 in enumerate(self.coordKeys):
                if jj == 0:
                    #
                    # Start
                    #
                    if ii == jj:
                        ind_full[ii] = copy.deepcopy(loc_ind)
                    else:
                        ind_full[ii] = np.ones([sliceFullShape[jj]], dtype=int)

                else:
                    #
                    # Outer multiplication
                    #
                    if ii == jj:
                        ind_full[ii] = np.outer(ind_full[ii], copy.deepcopy(loc_ind))
                    else:
                        ind_full[ii] = np.outer(
                            ind_full[ii], np.ones([sliceFullShape[jj]], dtype=int)
                        )

                    ind_full[ii] = ind_full[ii].reshape(sliceFullShape[0 : jj + 1])

        #
        # Assemble slice
        #
        flattened_indeces = inds2i(ind_full, self.shape).flatten()

        return flattened_indeces, sliceShape

    def getSlice(self, key, coord_treatment):
        """
        Get a slice of field given by `key` containing points from the full table.
        `coord_treatment` is a dictionary that specifies the treatment of specific coordinates by dictionaries:
            {"type": "ind", "ind": ind}:    only specific index
            {"type": "indlist", "indlist": array}:    only list of specific indeces
            {"type": "all"} or not present: do not limit this coordinate
        """
        #
        # Get indeces and shape
        #
        flattened_indeces, shape = self.getSliceIndeces(coord_treatment)

        #
        # Return slice and flattened indeces in full table
        #
        return (
            self.data[key].flatten()[flattened_indeces].reshape(shape),
            flattened_indeces,
        )

    def setSlice(self, key, coord_treatment, array):
        """
        Set a slice of field given by `key` with the elments of array.
        `coord_treatment` is a dictionary that specifies the treatment of specific coordinates by dictionaries:
            {"type": "ind", "ind": ind}:    only specific index
            {"type": "indlist", "indlist": array}:    only list of specific indeces
            {"type": "all"} or not present: do not limit this coordinate
        """
        array = np.array(array, dtype=self.dtype)

        #
        # Get indeces and shape
        #
        flattened_indeces, shape = self.getSliceIndeces(coord_treatment)

        if np.prod(array.shape) == np.prod(shape):
            #
            # Setting is possible
            #
            np.put(self.data[key], flattened_indeces, array)
        else:
            print(
                "Cannot assign array of shape {} to slice of shape {} in field {}".format(
                    array.shape, shape, key
                )
            )
            sys.stdout.flush()
