#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import os
import sys

import numpy as np


class BaseAlyaTable:
    """
    Base class to hold tabulated data for Alya.
    """

    def __init__(
        self, dic={}, keys=[], coordKeys=[], coords={}, typ="single", overwrite=True
    ):
        #
        # Initialize structure
        #
        self.coordKeys = []
        self.coords = {}
        self.shape = []
        self.dims = 0

        #
        # Initialize data
        #
        self.nkeys = 0
        self.__keys__ = []
        self.data = {}

        #
        # Single precision can speed up calculations substantially
        #
        if typ == "single":
            self.dtype = np.float32
        elif typ == "double":
            self.dtype = np.float64

        #
        # Add all present coordinates
        #
        for i, k in enumerate(coordKeys):
            self.addCoordinate(k, coords[k])

        #
        # Get all data
        #
        for i, k in enumerate(keys):
            self.addData(k, dic[k], overwrite=overwrite)

    def addCoordinate(self, key, coord):
        """
        Add coordinate to table
        """
        #
        # Add coordinate
        #
        self.coordKeys.append(key)
        self.coords[key] = copy.deepcopy(coord)

        #
        # Update shape
        #
        self.getShape()

    def getShape(self):
        """
        Get current shape based on coordinates
        """
        #
        # Get ndim
        #
        self.dims = len(self.coordKeys)

        #
        # Get shape
        #
        self.shape = []
        for k in self.coordKeys:
            self.shape.append(len(self.coords[k]))
        self.shape = tuple(self.shape)

        return self.shape

    def addData(self, key, dat, overwrite=True):
        """
        Add a new array to the table.
        If it is already taken, proceed according to `overwrite`
        """
        #
        # Check if field is already present
        #
        if key in self.keys():
            if overwrite:
                print("Overwriting {}".format(key))
            else:
                print("Not adding {} because it is already present".format(key))
                return

        #
        # Add coordinate
        #
        try:
            self.data[key] = copy.deepcopy(np.array(dat, dtype=self.dtype))
        except:
            print("Could not add {} as a numpy array".format(key))
            return

        if not key in self.keys():
            self.__keys__.append(key)

        #
        # Check if dimension is correct
        #
        if len(self.data[key].shape) != self.dims:
            print(
                "Incorrect number of dimensions in {}, tab:{}, data:{}".format(
                    key, self.dims, len(self.data[key].shape)
                )
            )
            self.removeData(key)
            return

        elif list(self.data[key].shape) != list(self.shape):
            print(
                "Incorrect shape in {}, tab:{}, data:{}".format(
                    key, self.shape, self.data[key].shape
                )
            )
            self.removeData(key)
            return

        #
        # Update nkeys
        #
        self.getNKeys()

    def addEmptyData(self, key, overwrite=True):
        """
        Add a data element with empty initial value
        """
        dat = np.empty(self.shape)
        self.addData(key, dat, overwrite=overwrite)

    def removeData(self, key):
        """
        Remove an array from the table.
        """
        #
        # Check if can be removed
        #
        if not key in self.data.keys():
            print("Cannot remove {}, it is not in the table.".format(key))
        else:
            del self.data[key]

        #
        # Check key list
        #
        if key in self.keys():
            ind = self.keys().index(key)
            del self.__keys__[ind]

        self.getNKeys()

    def getNKeys(self):
        """
        Get number of keys
        """
        #
        # Get ndim
        #
        self.nkeys = len(self.keys())

        return self.nkeys

    def keys(self):
        """
        Returns keys associted with table.
        """
        return self.__keys__

    def getGridOfCoordinate(self, ck):
        """
        Get an array corresponding to the value of the given coordinate in the full data shape
        """
        #
        # Return if not a coordinate
        #
        if not ck in self.coordKeys:
            print("{} is not a coordinate of the table".format(ck))
            return None

        ik = self.coordKeys.index(ck)
        real_location = [ik]

        #
        # Get shape of remaining coordinates
        #
        shapeloc = []
        for ii, key in enumerate(self.coordKeys):
            if key != ck:
                shapeloc.append(self.shape[ii])
                real_location.append(ii)

        #
        # Get array
        #
        dat = np.outer(self.coords[ck], np.ones(shapeloc, dtype=self.dtype)).reshape(
            [self.shape[ik]] + shapeloc
        )

        #
        # Put coordinates in correct order
        #
        dat = np.moveaxis(dat, range(self.dims), real_location)

        return dat

    def addGridOfCoordinate(self, ck, overwrite=True):
        """
        Add a data element corresponding to the local value of the coordinate
        """
        #
        # Return if not a coordinate
        #
        if not ck in self.coordKeys:
            print("{} is not a coordinate of the table".format(ck))
            return

        #
        # Get array
        #
        dat = self.getGridOfCoordinate(ck)

        #
        # Add array
        #
        self.addData(ck, dat, overwrite=overwrite)

    def addGridOfAllCoordinates(self, overwrite=True):
        """
        Add a data elements corresponding to each coordinate
        """
        for ck in self.coordKeys:
            self.addGridOfCoordinate(ck, overwrite=overwrite)

    def coordinatesToData(self):
        """
        Include multidimensional arrays representing the coordinates of the table.
        """
        self.addGridOfAllCoordinates()
