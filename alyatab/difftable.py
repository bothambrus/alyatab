#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import sys

import numpy as np

from alyatab.interptable import InterpAlyaTable


class DifferentiatingAlyaTable(InterpAlyaTable):
    """
    Class to hold tabulated data for Alya with numerical differentiation capabilities.
    """

    def __init__(
        self, dic={}, keys=[], coordKeys=[], coords={}, typ="single", overwrite=True
    ):
        #
        # Initialize parent class
        #
        InterpAlyaTable.__init__(
            self,
            dic=dic,
            keys=keys,
            coordKeys=coordKeys,
            coords=coords,
            typ=typ,
            overwrite=overwrite,
        )

    def partialDifferentiationAlongOneCoordinate(
        self, ck, key, diffkey, scheme="central"
    ):
        """
        Differentiate field `key` of `self.data` along the `ck` coordinate, and save result in `diffkey` field.
        """
        #
        # Exit if irrelevant input is given
        #
        if not ck in self.coordKeys:
            print(
                "Key {} is not among table coordinates for differentiation.".format(ck)
            )
            sys.stdout.flush()
            return
        if not key in self.keys():
            print("Key {} is not among table data for differentiation.".format(key))
            sys.stdout.flush()
            return

        #
        # Add grid of coordinate of differentiation
        #
        self.addGridOfCoordinate(ck, overwrite=False)

        #
        # Initialize result array
        #
        self.addEmptyData(diffkey, overwrite=False)

        #
        # Index of coordinate along which to differentiate
        #
        ick = self.coordKeys.index(ck)

        #
        # Derivate
        #
        if scheme == "forward" or scheme == "backward":
            #
            # Do diffs
            #
            dx = np.diff(self.data[ck], axis=ick)
            dy = np.diff(self.data[key], axis=ick)

            #
            # Derivative of first points
            #
            gradi = dy / dx

        elif scheme == "central":
            #
            # First slice the data
            # For this we need slicing with ranges
            #
            x1, shapeND = self.getSlice(
                ck,
                {
                    ck: {
                        "type": "indlist",
                        "indlist": range(0, len(self.coords[ck]) - 1),
                    }
                },
            )
            x2, shapeND = self.getSlice(
                ck, {ck: {"type": "indlist", "indlist": range(1, len(self.coords[ck]))}}
            )
            dx1 = np.diff(x1, axis=ick)
            dx2 = np.diff(x2, axis=ick)

            ycm1, shapeND = self.getSlice(
                key,
                {
                    ck: {
                        "type": "indlist",
                        "indlist": range(0, len(self.coords[ck]) - 2),
                    }
                },
            )
            yc, shapeND = self.getSlice(
                key,
                {
                    ck: {
                        "type": "indlist",
                        "indlist": range(1, len(self.coords[ck]) - 1),
                    }
                },
            )
            ycp1, shapeND = self.getSlice(
                key,
                {ck: {"type": "indlist", "indlist": range(2, len(self.coords[ck]))}},
            )

            #
            # Gradient according to central scheme
            #
            gradi = (
                (ycp1 * dx1 / (dx2 * (dx1 + dx2)))
                + (yc * (dx2 - dx1) / (dx1 * dx2))
                - (ycm1 * dx2 / (dx1 * (dx1 + dx2)))
            )

        #
        # Get last gradient and append
        #
        if scheme == "forward" or scheme == "central":
            dxedge, dyedge = self.getEdgeDifference(ck, key, len(self.coords[ck]) - 2)
            gradi = np.append(gradi, dyedge / dxedge, axis=ick)

        #
        # Get first gradient and append
        #
        if scheme == "backward" or scheme == "central":
            dxedge, dyedge = self.getEdgeDifference(ck, key, 0)
            gradi = np.append(dyedge / dxedge, gradi, axis=ick)

        #
        # Assign
        #
        self.data[diffkey] = gradi

    def getEdgeDifference(self, ck, key, locLow):
        """
        Return the differences of field `key` of `self.data` and `ck` along the `ck` coordinate between `locLow` and `locLow+1`.
        The `key` may be None, to avoid differentiation along y.
        """
        #
        # Index of coordinate along which to differentiate
        #
        ick = self.coordKeys.index(ck)

        #
        # Get indeces of low
        #
        coord_treatment = {}
        coord_treatment[ck] = {}
        coord_treatment[ck]["type"] = "ind"
        coord_treatment[ck]["ind"] = locLow
        flattened_indecesLow, shapeND = self.getSliceIndeces(coord_treatment)

        #
        # Get indeces of high
        #
        coord_treatment = {}
        coord_treatment[ck] = {}
        coord_treatment[ck]["type"] = "ind"
        coord_treatment[ck]["ind"] = locLow + 1
        flattened_indecesLowP1, shapeND = self.getSliceIndeces(coord_treatment)

        #
        # Get x
        #
        xLow = self.data[ck].flatten()[flattened_indecesLow].reshape(shapeND)
        xLowP1 = self.data[ck].flatten()[flattened_indecesLowP1].reshape(shapeND)

        #
        # Get y
        #
        if not key is None:
            yLow = self.data[key].flatten()[flattened_indecesLow].reshape(shapeND)
            yLowP1 = self.data[key].flatten()[flattened_indecesLowP1].reshape(shapeND)

        #
        # Edge differences
        #
        shapeEdge = copy.deepcopy(list(shapeND))
        shapeEdge.insert(ick, 1)
        dx = xLowP1 - xLow
        dx = dx.reshape(shapeEdge)
        if not key is None:
            dy = yLowP1 - yLow
            dy = dy.reshape(shapeEdge)

        if not key is None:
            return dx, dy
        else:
            return dx

    def partialDifferentiation(self, ck_list, key, diffkey, scheme="central"):
        """
        Differentiate field `key` of `self.data` along the multiple coordinates, and save result in `diffkey` field.
        """

        #
        # Initialize result array
        #
        self.addEmptyData(diffkey, overwrite=False)
        self.data[diffkey] = self.data[key]

        #
        # Repeat differentiation
        #
        for ck in ck_list:
            self.partialDifferentiationAlongOneCoordinate(
                ck, diffkey, diffkey, scheme=scheme
            )
