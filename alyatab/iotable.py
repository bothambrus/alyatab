#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import os
import sys
import time

import numpy as np

from alyatab.clip import clipUnderAndOverflow
from alyatab.index import i2inds
from alyatab.parsing import searchExpression, stripCharactersFromString
from alyatab.slicetable import SliceAlyaTable


def writeAndAggregateContent(f, header, string):
    """
    Write and aggregate content
    """
    f.write(string)
    header += "{}".format(string)
    return header


class IOAlyaTable(SliceAlyaTable):
    """
    Class to hold tabulated data for Alya with I/O capabilities.
    """

    def __init__(
        self, dic={}, keys=[], coordKeys=[], coords={}, typ="single", overwrite=True
    ):
        #
        # Initialize parent class
        #
        SliceAlyaTable.__init__(
            self,
            dic=dic,
            keys=keys,
            coordKeys=coordKeys,
            coords=coords,
            typ=typ,
            overwrite=overwrite,
        )

    def __isFloat__(self, value):
        """
        Check if string can be interpreted as float.
        """
        try:
            float(value)
            return True
        except:
            return False

    def readText(self, fn, searchInComments=False, verbose=True):
        """
        Read table from text file.

        equivalent of old readDictFromFile
        """
        try:
            initime = time.perf_counter()
        except:
            initime = time.time()
        #
        # Print
        #
        if verbose:
            print("Reading table:{}".format(fn))
            sys.stdout.flush()

        if searchInComments:
            stripComment = ["\$"]
        else:
            stripComment = []

        with open(fn, "r") as f:
            #
            # READ TABLE DIMENSIONS
            #
            data, nline = searchExpression(
                f,
                expr="N_VALUES",
                position=0,
                stripNonAlphaNum=True,
                returnToTop=True,
                stripComment=stripComment,
            )
            if not (data is None):
                nValues = [int(s.rstrip(",")) for s in data[1:]]
            else:
                raise ValueError("Cannot find {} in {}".format("N_VALUES", fn))

            #
            # table division
            #
            dataPerLine, nline = searchExpression(
                f,
                expr="N_CHUNK_DATA_PER_LINE",
                position=0,
                stripNonAlphaNum=True,
                returnToTop=True,
                stripComment=stripComment,
            )
            dataPerLine = dataPerLine[1]
            if dataPerLine is None:
                dataPerLine = 15
            else:
                dataPerLine = int(dataPerLine)

            self.dims = len(nValues)
            self.shape = nValues
            self.coords = {}
            self.coordKeys = []
            f.seek(0)

            #
            # READ TABLE COORDINATES
            #
            for i, n in enumerate(nValues):
                if n == 0 or n == 1:
                    # one value along coordinate
                    self.coords[i] = np.ones([1], dtype=self.dtype)
                    self.coordKeys.append("")
                    self.shape[i] = 1
                else:
                    #
                    # Get coordinate name
                    #
                    data, nline = searchExpression(
                        f,
                        expr="SUBDIVISION",
                        position=1,
                        stripNonAlphaNum=True,
                        returnToTop=False,
                        stripComment=stripComment,
                    )
                    key = stripCharactersFromString(data[0], stripComment)
                    self.coordKeys.append(key)

                    #
                    # Read coordinate
                    #
                    self.coords[key] = []
                    crit = True
                    while crit:
                        data = stripCharactersFromString(
                            f.readline(), stripComment
                        ).split()

                        if len(data) == 0 or not self.__isFloat__(data[0]):
                            crit = False
                        else:
                            self.coords[key] = np.append(
                                self.coords[key],
                                np.array([float(d) for d in data], dtype=self.dtype),
                            )

            #
            # Convert shape to tuple
            #
            self.shape = tuple(self.shape)

            #
            # READ HEADER LINE
            #
            for i, ck in enumerate(self.coordKeys):
                if ck != "":
                    targetKey = ck
                    targetInd = i
                break

            data, nline = searchExpression(
                f,
                expr=targetKey,
                position=targetInd,
                stripNonAlphaNum=True,
                returnToTop=False,
                stripComment=stripComment,
            )

            #
            # Find missing coordKeys
            #
            for i, ck in enumerate(self.coordKeys):
                if ck in "":
                    self.coordKeys[i] = stripCharactersFromString(data[i], stripComment)

            #
            # Find keys
            #
            self.__keys__ = []
            for d in data[self.dims :]:
                self.__keys__.append(d)
            self.nkeys = len(self.keys())

            numLine = int(
                np.ceil(float(len(self.keys()) + self.dims) / float(dataPerLine))
            )

            #
            # READ ENTRIES
            #
            data, nline1 = searchExpression(
                f,
                expr=targetKey,
                position=targetInd,
                stripNonAlphaNum=True,
                returnToTop=True,
                stripComment=stripComment,
            )
            data, nline2 = searchExpression(
                f,
                expr=targetKey,
                position=targetInd,
                stripNonAlphaNum=True,
                returnToTop=False,
                stripComment=stripComment,
            )

            # next line is already in the table
            self.data = {}
            for k in self.keys():
                self.data[k] = np.empty(self.shape, dtype=self.dtype)

            if dataPerLine >= (len(self.keys()) + self.dims):
                #
                # Read all data in one step
                #
                print(
                    "  Skipping the first {} lines of the textfile as header".format(
                        nline1 + nline2
                    )
                )
                alldata = np.loadtxt(fn, skiprows=nline1 + nline2, dtype=self.dtype)

                #
                # Convert all data at once
                #
                for j, k in enumerate(self.keys()):
                    #
                    # Print
                    #
                    if (j % max((len(self.keys()) // 10), 1)) == 0 and verbose:
                        print(
                            "  Reading progress: {:3d}% ({})".format(
                                int(np.ceil(100 * float(j) / len(self.keys()))),
                                os.path.basename(fn),
                            )
                        )
                        sys.stdout.flush()

                    #
                    # Process
                    #
                    self.data[k] = np.reshape(alldata[:, self.dims + j], self.shape)

            else:
                #
                # LEGACY READING
                #
                i = 0
                while data:
                    if i == np.prod(self.shape):
                        break

                    #
                    # Print
                    #
                    if (i % (np.prod(self.shape) // 10)) == 0 and verbose:
                        print(
                            "  Reading progress: {:3d}% ({})".format(
                                int(np.ceil(100 * float(i) / np.prod(self.shape))),
                                os.path.basename(fn),
                            )
                        )
                        sys.stdout.flush()

                    ind = i2inds(i, self.shape)
                    line = []
                    for j in range(numLine):
                        data = f.readline().split()
                        if j == 0:
                            line += data[self.dims :]
                        else:
                            line += data

                    for j, k in enumerate(self.keys()):
                        self.data[k][ind] = float(line[j])

                    i += 1

            #
            # Print timer
            #
            try:
                endtime = time.perf_counter()
            except:
                endtime = time.time()
            print("Reading done in {:6.2f} s".format(endtime - initime))

    def writeText(
        self,
        fn,
        write_keys="all",
        coordOrder="default",
        dataPerLine=50,
        verbose=True,
        clipUnderAndOver=True,
        commentsToColumns={},
    ):
        """
        Write table to text file.
        """
        try:
            initime = time.perf_counter()
        except:
            initime = time.time()
        #
        # Print
        #
        if verbose:
            print("Writing table:{}".format(fn))
            sys.stdout.flush()

        #
        # Decide if the table needs reshaping
        #
        if coordOrder == "default":
            #
            # Write in original shape
            #
            locCoordOrder = self.coordKeys
            locShape = self.shape

        else:
            #
            # Write in new shape
            #
            locCoordOrder = []
            locShape = []
            for co in coordOrder:
                if co in self.coordKeys:
                    locCoordOrder.append(co)
                    locShape.append(len(self.coords[co]))

        #
        # Decide what to write
        #
        if write_keys == "all":
            #
            # Write all keys
            #
            write_keys = copy.deepcopy(self.__keys__)
        else:
            #
            # Remove keys that are not present
            #
            for ik, key in enumerate(write_keys):
                if not (key in self.__keys__):
                    del write_keys[ik]

        header = ""
        with open(fn, "w") as f:
            #
            # Dimensions
            #
            header = writeAndAggregateContent(f, header, "N_VALUES: ")
            for j, key in enumerate(locCoordOrder):
                if j == len(locCoordOrder) - 1:
                    comma = ""
                else:
                    comma = ","

                dimlen = len(self.coords[key])

                header = writeAndAggregateContent(
                    f, header, "{}{} ".format(dimlen, comma)
                )
            header = writeAndAggregateContent(f, header, "\n\n")

            #
            # Number of variables
            #
            header = writeAndAggregateContent(
                f, header, "N_VARIABLES: {}\n\n".format(len(write_keys))
            )

            #
            # Maximum chunk size
            #
            header = writeAndAggregateContent(
                f, header, "N_CHUNK_DATA_PER_LINE: {}\n\n".format(dataPerLine)
            )

            #
            # Coordinate discretizations
            #
            for key in locCoordOrder:
                header = writeAndAggregateContent(
                    f, header, "\n{} SUBDIVISION:".format(key)
                )
                for i, xval in enumerate(self.coords[key]):
                    if i % dataPerLine == 0:
                        header = writeAndAggregateContent(f, header, "\n")
                    xval = clipUnderAndOverflow(xval, clipUnderAndOver)
                    header = writeAndAggregateContent(f, header, "{:.8e} ".format(xval))
                header = writeAndAggregateContent(f, header, "\n\n")

            #
            # Write header numbering
            #
            header = writeAndAggregateContent(f, header, "\n")
            ih = 1
            for k, key in enumerate(locCoordOrder):
                #
                # Get coordiante, name, and additional comments
                #
                addition = ""
                if key in commentsToColumns.keys():
                    addition = ", {}".format(commentsToColumns[key])
                message = "{:10} coordiante #{}{}".format(key, k + 1, addition)

                #
                # Write
                #
                header = writeAndAggregateContent(
                    f, header, "$$  {:>4d}: {}\n".format(ih, message)
                )
                ih += 1

            for k, key in enumerate(write_keys):
                #
                # Get variable, name, and additional comments
                #
                addition = ""
                if key in commentsToColumns.keys():
                    addition = ", {}".format(commentsToColumns[key])
                message = "{:10} variable #{}{}".format(key, k + 1, addition)

                #
                # Write
                #
                header = writeAndAggregateContent(
                    f, header, "$$  {:>4d}: {}\n".format(ih, message)
                )
                ih += 1

            #
            # Write header line
            #
            header = writeAndAggregateContent(f, header, "\n")

            for k, key in enumerate(locCoordOrder):
                header = writeAndAggregateContent(f, header, "{}    ".format(key))

            for key in write_keys:
                header = writeAndAggregateContent(f, header, "{}    ".format(key))

        #
        # Write data entries
        #
        if (
            dataPerLine >= (len(self.keys()) + self.dims)
            and locCoordOrder == self.coordKeys
        ):
            #
            # Write in bulk
            #
            alldata = np.empty([np.prod(self.shape), len(write_keys) + self.dims])
            #
            # Add coordinates
            #
            for ii, ck in enumerate(self.coordKeys):
                dat = self.getGridOfCoordinate(ck)
                alldata[:, ii] = dat.flatten()

            #
            # Convert all data
            #
            for j, k in enumerate(write_keys):
                alldata[:, self.dims + j] = self.data[k].flatten()

            #
            # Write
            #
            np.savetxt(fn, alldata, fmt="%.8e", header=header, comments="")

        else:
            #
            # LEGACY WRITING
            #
            with open(fn, "a") as f:
                for i in range(np.prod(locShape)):
                    #
                    # Print
                    #
                    if (i % (np.prod(locShape) // 10)) == 0 and verbose:
                        print(
                            "  Writing progress: {:3d}% ({})".format(
                                int(np.ceil(100 * float(i) / np.prod(locShape))),
                                os.path.basename(fn),
                            )
                        )
                        sys.stdout.flush()

                    #
                    # Get indeces along individual coordinates
                    #
                    locInd = i2inds(i, locShape)

                    if coordOrder == "default":
                        ind = locInd
                    else:
                        ind = []
                        for key in self.coordKeys:
                            ind.append(locInd[locCoordOrder.index(key)])
                        ind = tuple(ind)

                    countData = 0
                    for j, key in enumerate(locCoordOrder):
                        if countData % dataPerLine == 0:
                            f.write("\n")
                        countData += 1
                        xval = clipUnderAndOverflow(
                            self.coords[key][locInd[j]], clipUnderAndOver
                        )
                        f.write("{:.8e} ".format(xval))

                    for key in write_keys:
                        if countData % dataPerLine == 0:
                            f.write("\n")
                        countData += 1
                        xval = clipUnderAndOverflow(
                            self.data[key][ind], clipUnderAndOver
                        )
                        f.write("{:.8e} ".format(xval))

        #
        # Print timer
        #
        try:
            endtime = time.perf_counter()
        except:
            endtime = time.time()
        print("Writing done in {:6.2f} s".format(endtime - initime))
