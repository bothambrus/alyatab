#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################

import copy
import json
import os

import numpy as np
from matplotlib import cm, colors, style, ticker
from matplotlib.cm import ScalarMappable


def colfun(param, name="viridis", nlevel=256):
    """
    Get viridis color for 0 <= param <= 1.
    """
    cmap = getColormap(name=name, nlevel=nlevel)
    return cmap(param)


def getColormap(name="viridis", nlevel=256):
    return cm.get_cmap(name, nlevel)


def getStyles():
    """
    Get styles for plotting.
    """
    style.use("default")
    font_style = dict(family="Sans", color="black", weight="normal", size=12)

    default_line_style = dict(
        marker=".",
        linestyle="-",
        fillstyle="none",
        color="black",
        linewidth=1.0,
        markersize=0,
    )

    return font_style, default_line_style


def setPltBackend(backend="default"):
    import matplotlib

    if backend != "default":
        matplotlib.use(backend)
        print("Setting matplotlib backend to {}".format(backend))
    import matplotlib.pyplot as plt

    return plt


def adjustMarginToFitLabels(fig, ax):
    """
    Adjust figure margins to fit label
    """
    yticklabels = ax.get_yticklabels()
    maxYtickLabLengt = 0
    for ytl in yticklabels:
        maxYtickLabLengt = max(
            maxYtickLabLengt, len("{:g}".format(float(ytl.get_text())))
        )
    fig.subplots_adjust(left=0.018 * maxYtickLabLengt + 0.07)

    return fig


def formatAxisTicks(ax, font_style):
    """
    Generic formatting of axes.
    """
    ax.xaxis.set_major_locator(ticker.MaxNLocator(5))
    locs = ax.get_xticks()
    ax.xaxis.set_major_locator(ticker.FixedLocator(locs))
    ax.set_xticklabels(locs, fontdict=font_style)
    ax.xaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))

    ax.yaxis.set_major_locator(ticker.MaxNLocator(5))
    locs = ax.get_yticks()
    ax.yaxis.set_major_locator(ticker.FixedLocator(locs))
    ax.set_yticklabels(locs, fontdict=font_style)
    ax.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))

    try:
        ax.zaxis.set_major_locator(ticker.MaxNLocator(5))
        locs = ax.get_zticks()
        ax.zaxis.set_major_locator(ticker.FixedLocator(locs))
        ax.set_zticklabels(locs, fontdict=font_style)
        ax.zaxis.set_major_formatter(ticker.StrMethodFormatter("{x:g}"))
    except:
        pass

    return ax


def getLatexKeyAndUnit(type, key):
    """
    Returns a latex-formatted key corresponding to a property.
    """
    keyLatex = key
    unit = "?"
    if type == "prop":
        if key == "Z":
            unit = "-"
        elif key == "ZMEAN":
            keyLatex = "\widetilde{{Z}}"
            unit = "-"
        elif key == "ZVAR":
            keyLatex = "Z_v"
            unit = "-"
        elif key == "x":
            unit = "m"
        elif key == "C":
            unit = "-"
        elif key == "CMEAN":
            keyLatex = "\widetilde{{C}}"
            unit = "-"
        elif key == "CVAR":
            keyLatex = "C_v"
            unit = "-"
        elif key == "I":
            keyLatex = "i"
            unit = "-"
        elif key == "IMEAN":
            keyLatex = "\widetilde{{i}}"
            unit = "-"
        elif key == "EqRat":
            keyLatex = "\phi"
            unit = "-"
        elif key == "Lambda":
            keyLatex = "\lambda"
            unit = "-"
        elif key == "Yc":
            keyLatex = "Y_c"
            unit = "-"
        elif key == "chi":
            keyLatex = "\chi"
            unit = "1/s"
        elif key == "Dt":
            keyLatex = "D_t"
            unit = "m^2/s"
        elif key == "Dfuel":
            keyLatex = "D_{{fuel}}"
            unit = "m^2/s"
        elif key == "HeatRel":
            keyLatex = "\dot{{\omega}}_{{T}}"
            unit = "J/(m^3s)"
        elif key == "omegaYc":
            keyLatex = "\dot{{\omega}}_{{Y_c}}"
            unit = "kg/(m^3s)"
        elif key == "omegaC":
            keyLatex = "\dot{{\omega}}_{{C}}"
            unit = "kg/(m^3s)"
        elif key == "nativeStrain":
            keyLatex = "a"
            unit = "1/s"
        elif key == "zgrad":
            keyLatex = "\\nabla Z"
            unit = "1/m"
        elif key == "cgrad":
            keyLatex = "\\nabla C"
            unit = "1/m"
        elif key == "T":
            unit = "K"
        elif key == "overlap":
            unit = "-"

    if type == "speci" or "Yk_" == key[0:3]:
        if "Yk_" == key[0:3]:
            sp = key.rpartition("Yk_")[-1]
        else:
            sp = key
        keyLatex = "Y_{{{}}}".format(sp)
        unit = "-"

    if type == "source" or "omegak_" == key[0:7]:
        if "omegak_" == key[0:7]:
            sp = key.rpartition("omegak_")[-1]
        else:
            sp = key
        keyLatex = "\dot{{\omega}}_{{{}}}".format(sp)
        unit = "kg/(m^3s)"

    return keyLatex, unit


def addColorbarAsAxis(
    fig,
    cmin,
    cmax,
    barx=0.82,
    bary=0.2,
    bardx=0.01,
    bardy=0.6,
    barlabel="",
    barorientation="vertical",
    adjustmargin=True,
    mapname="viridis",
    mapnlevel=256,
    cscale="linear",
):
    """
    Add a colorbar to a figure with given location and properties.
    Color limits: `cmin`, `cmax`
    """
    plt = setPltBackend()

    #
    # Create color map
    #
    if cscale == "log":
        norm = colors.LogNorm(cmin, cmax)
    else:
        norm = plt.Normalize(cmin, cmax)

    cmappable = ScalarMappable(norm, cmap=getColormap(name=mapname, nlevel=mapnlevel))
    cmappable.set_array(np.linspace(cmin, cmax, 2))

    #
    # Decide location
    #
    cb_ax = fig.add_axes([barx, bary, bardx, bardy])

    if adjustmargin:
        if barorientation == "vertical":
            #
            # Vertical, put either left or right
            #
            if barx > 0.5:
                fig.subplots_adjust(right=barx - 0.02)
            else:
                fig.subplots_adjust(left=barx + 0.15)

        elif barorientation == "horizontal":
            #
            # Horizontal, put either top or bottom
            #
            if bary > 0.5:
                fig.subplots_adjust(top=bary - 0.1)
            else:
                fig.subplots_adjust(bottom=bary + 0.2)

    #
    # Add color bar
    #
    cb = fig.colorbar(
        cmappable,
        cax=cb_ax,
        label=barlabel,
        orientation=barorientation,
    )
