#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy

import numpy as np


def i2inds(i, dimlen):
    """
    Determine indeces from flattened index and shape.
    """
    dims = len(dimlen)
    inds = np.empty([dims], dtype=int)

    if dims == 1:
        inds[0] = i
    else:
        iAux = i
        # last index is the remainder with last size
        for j in range(dims - 1, -1, -1):
            inds[j] = iAux % dimlen[j]
            iAux = iAux // dimlen[j]
    return tuple(inds)


def inds2i(inds, dimlen):
    """
    Determine flattened index from indeces and shape.
    """
    dims = len(dimlen)
    i = inds[0]
    for j in range(1, dims):
        i = inds[j] + i * dimlen[j]
    return i


def i2inds_vec(i, dimlen):
    """
    Determine indeces from an array of flattened indeces and shape.
    i[:]
    inds[0:dims,:]
    """
    dims = len(dimlen)
    inds = np.empty([len(i), dims], dtype=int)

    if dims == 1:
        inds[:, 0] = i[:]
    else:
        iAux = i
        # last index is the remainder with last size
        for j in range(dims - 1, -1, -1):
            inds[:, j] = iAux % dimlen[j]
            iAux = iAux // dimlen[j]
    return inds


def inds2i_vec(inds, dimlen):
    """
    Determine flattened index from indeces and shape.
    The second index of inds is the dimension, first is the vectorization.
    """
    dims = len(dimlen)
    i = copy.deepcopy(inds[:, 0])
    for j in range(1, dims):
        i = inds[:, j] + i * dimlen[j]
    return i
