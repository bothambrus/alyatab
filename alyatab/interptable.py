#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import os
import sys

import numpy as np

from alyatab.index import i2inds, i2inds_vec, inds2i_vec
from alyatab.iotable import IOAlyaTable


class InterpAlyaTable(IOAlyaTable):
    """
    Class to hold tabulated data for Alya with interpolation capabilities.
    """

    def __init__(
        self, dic={}, keys=[], coordKeys=[], coords={}, typ="single", overwrite=True
    ):
        #
        # Initialize parent class
        #
        IOAlyaTable.__init__(
            self,
            dic=dic,
            keys=keys,
            coordKeys=coordKeys,
            coords=coords,
            typ=typ,
            overwrite=overwrite,
        )

    def __getIndex(self, coord, v, sortedcoord=True):
        """
        Find the indexes of neghboring coordinates of value `v` in vector `coord`
        """
        #
        # Duplicate code for performance
        #
        if not sortedcoord:
            #
            # Sort the array first
            #
            correctOrder = np.argsort(coord)

            #
            # Look for nearest points:
            #
            indUpper = min(np.searchsorted(coord[correctOrder], v), len(coord) - 1)
            if indUpper == 0:
                if len(coord) == 1:
                    ret = [0, 0]
                else:
                    ret = [0, 1]
            else:
                ret = [indUpper - 1, indUpper]

            #
            # Undo sorting
            #
            ret[0] = correctOrder[ret[0]]
            ret[1] = correctOrder[ret[1]]
        else:
            #
            # Look for nearest points in sorted array:
            #
            indUpper = min(np.searchsorted(coord, v), len(coord) - 1)

            #
            # Treat edge cases
            #
            if indUpper == 0:
                if len(coord) == 1:
                    ret = [0, 0]
                else:
                    ret = [0, 1]
            else:
                ret = [indUpper - 1, indUpper]

        return tuple(ret)

    def interpolate(self, val, key_list, unscaledVar=None, allowExtrapolate=True):
        """
        Interpolate in table based on given scaled control variables.
        `val`: array of scaled control variables with length `self.dims`
        `key_list`: list of variables to interpolate
        `unscaledVar`: may define one scaled-unscaled pair,
        to do the lookup based on the unscaled control variables.
        e.g.: {"C": "Yc"}
        """
        #
        # Initialize variables
        # Neighboring indeces
        # Weights of neighbours
        # Shape of hypercube
        # Hypercube data
        # Weights of hypercube
        #
        indL = np.empty_like(val, dtype=int)
        indU = np.empty_like(val, dtype=int)
        weightL = np.empty_like(val, dtype=self.dtype)
        weightU = np.empty_like(val, dtype=self.dtype)
        hyperShape = np.ones_like(val, dtype=int) * 2
        n_hyper_vertex = np.prod(hyperShape)
        tableData = np.empty([len(key_list), n_hyper_vertex], dtype=self.dtype) * np.nan
        weights = np.ones([n_hyper_vertex], dtype=self.dtype)
        extrapolated = False

        #
        # Get 1D indexes and weights
        #
        for ick, v in enumerate(val):
            coordName = self.coordKeys[ick]

            if (unscaledVar is None) or (not coordName in unscaledVar.keys()):
                #
                # Find nearest indeces in this coordinate
                #
                (indL[ick], indU[ick]) = self.__getIndex(self.coords[coordName], v)
                xL = self.coords[coordName][indL[ick]]
                xU = self.coords[coordName][indU[ick]]

                #
                # Clip weight between 0 and 1 to disallow extrapolation
                #
                weightU[ick] = (v - xL) / (max(xU - xL, 1e-20))
                if weightU[ick] < -1e-20 or weightU[ick] > (1.0 + 1e-20):
                    extrapolated = True
                weightU[ick] = min(1.0, max(0.0, weightU[ick]))

        weightL = -1.0 * weightU + 1.0

        #
        # Get 1D indeces and weigths for the unscaled variable
        #
        if not unscaledVar is None:
            coordName = list(unscaledVar.keys())[0]
            iCoord = self.coordKeys.index(coordName)
            unscaledName = unscaledVar[coordName]

            #
            # Get data shape for creating a single vector of the unscaled variable
            #
            unscaledHyperShape = np.ones(len(val) - 1, dtype=int) * 2

            #
            # Pre-compute all possible hypercube indeces
            #
            indHC_array = i2inds_vec(
                range(np.prod(unscaledHyperShape)), unscaledHyperShape
            )

            #
            # Get data of unscaled vector
            #
            indFull = np.empty_like(val, dtype=int)
            unscaledData = []
            for j in range(np.prod(unscaledHyperShape)):
                unscaledData.append(np.empty(self.shape[iCoord]))

            for j in range(np.prod(unscaledHyperShape)):
                #
                # Define indeces of the vertex
                #
                indHC = indHC_array[j, :]
                jloc = 0
                for ick, ck in enumerate(self.coordKeys):
                    if ck != coordName:
                        indFull[ick] = (1 - indHC[jloc]) * indL[ick] + indHC[
                            jloc
                        ] * indU[ick]
                        jloc += 1

                #
                # Loop through remaining vertices
                #
                for i in range(self.shape[iCoord]):
                    indFull[iCoord] = i

                    unscaledData[j][i] = self.data[unscaledName][tuple(indFull)]

            #
            # Get unscaled vector
            #
            unscaled = np.zeros_like(self.coords[coordName])
            for j in range(np.prod(unscaledHyperShape)):
                indHC = indHC_array[j, :]
                weight = 1.0
                jloc = 0
                for ick, ck in enumerate(self.coordKeys):
                    if ck != coordName:
                        weight *= (1 - indHC[jloc]) * weightL[ick] + indHC[
                            jloc
                        ] * weightU[ick]
                        jloc += 1

                unscaled += weight * unscaledData[j]

            #
            # Find nearest indeces
            #
            (indL[iCoord], indU[iCoord]) = self.__getIndex(
                unscaled, val[iCoord], sortedcoord=False
            )
            xL = unscaled[indL[iCoord]]
            xU = unscaled[indU[iCoord]]

            #
            # Clip weight between 0 and 1 to disallow extrapolation
            #
            weightU[ick] = (val[iCoord] - xL) / (max(xU - xL, 1e-20))
            if weightU[ick] < -1e-20 or weightU[ick] > (1.0 + 1e-20):
                extrapolated = True
            weightU[ick] = min(1.0, max(0.0, weightU[ick]))
            weightL[iCoord] = 1.0 - weightU[iCoord]

        #
        # Pre-compute all possible hypercube indeces
        #
        ind_array = i2inds_vec(np.arange(n_hyper_vertex, dtype=int), hyperShape)

        #
        # Pre-compute all indeces and weights of hypercube
        #
        indInDict_array = np.multiply(ind_array, indU) + np.multiply(
            (1 - ind_array), indL
        )
        weights = np.prod(
            np.multiply(ind_array, weightU) + np.multiply((1 - ind_array), weightL),
            axis=-1,
        )

        #
        # Precompute flattened index in array
        #
        iInDict_array = inds2i_vec(indInDict_array, self.shape)

        #
        # Get data of this vertex
        #
        for ikey, key in enumerate(key_list):
            if key in self.coordKeys:
                #
                # Save scaled coordinate value of vertex
                #
                for i in range(n_hyper_vertex):
                    tableData[ikey, i] = self.coords[key][
                        indInDict_array[i, self.coordKeys.index(key)]
                    ]

            elif key in self.keys():
                #
                # Save data of vertex if available
                #
                tableData[ikey, :] = self.data[key].flatten()[iInDict_array]

        #
        # Multiply weights with table data and return
        #
        interpData = np.inner(tableData, weights)

        returnData = {}
        if (not extrapolated) or allowExtrapolate:
            for ikey, key in enumerate(key_list):
                returnData[key] = interpData[ikey]
        else:
            for key in key_list:
                returnData[key] = np.nan

        #
        # Return extrapolated status if unscaled control variable is used
        #
        if not unscaledVar is None:
            if extrapolated:
                returnData["extrapolated"] = 1
            else:
                returnData["extrapolated"] = 0

        return returnData

    def interpolateArray(
        self, valarr, key_list, unscaledVar=None, allowExtrapolate=True, verbose=False
    ):
        """
        Interpolate in table based on an arry of given scaled control variables.
        `valarr`: array-like that has the list of control varialbes in the last dimension.
        `key_list`: list of variables to interpolate
        `unscaledVar`: may define one scaled-unscaled pair,
        to do the lookup based on the unscaled control variables.
        e.g.: {"C": "Yc"}
        """
        #
        # Get shape of variables of array
        #
        if len(valarr.shape) > 1:
            shape = valarr.shape[0:-1]
        else:
            shape = (1,)
        flatshape = (np.prod(shape),)

        #
        # Initialize return data
        #
        returnData = {}
        for key in key_list:
            returnData[key] = np.empty(flatshape)
        if not unscaledVar is None:
            returnData["extrapolated"] = np.empty(flatshape)

        #
        # Flatten valarr
        #
        flatvalarr = np.reshape(valarr, (flatshape[0], self.dims))

        if verbose:
            talkInterval = np.prod(shape) // 20

        #
        # Loop through all points
        #
        for i in range(np.prod(shape)):
            #
            # Get value
            #
            val = flatvalarr[i, :]

            #
            # Interpolate
            #
            data = self.interpolate(
                val,
                key_list,
                unscaledVar=unscaledVar,
                allowExtrapolate=allowExtrapolate,
            )

            #
            # Save
            #
            for key in data.keys():
                if key in returnData.keys():
                    returnData[key][i] = data[key]

            #
            # Output
            #
            if verbose:
                if i % talkInterval == 0:
                    print(
                        "Interpolating data: {:>10}%".format(
                            int(100 * float(i) / float(np.prod(shape)))
                        )
                    )
                    sys.stdout.flush()

        #
        # Reshape to original shape
        #
        for key in key_list:
            if key in returnData.keys():
                returnData[key] = np.reshape(returnData[key], shape)

        #
        # Output
        #
        if verbose:
            print("Done interpolating data")
            sys.stdout.flush()

        return returnData

    @classmethod
    def get_subtable(
        cls,
        inst,
        coord_treatment,
        key_list="all",
        verbose=False,
        typ=None,
        allowExtrapolate=True,
    ):
        """
        Get a subtable containing points from the full table.
        `coord_treatment` is a dictionary that specifies the treatment of specific coordinates by dictionaries:
            {"type": "ind", "ind": ind}:    only specific index
            {"type": "const", "val": val}:  only a constant value (interpolation needed)
            {"type": "constUnscaled", "unscaledKey": key, "val": val}:  only a constant value of the unscaled variable corresponding to the scaled one (interpolation needed)
            {"type": "all"} or not present: do not limit this coordinate
        """

        #
        # Determine data type
        #
        if typ is None:
            if inst.dtype == np.float32:
                typ = "single"
            elif inst.dtype == np.float64:
                typ = "double"

        #
        # Determine requested keys
        #
        if isinstance(key_list, str) and key_list == "all":
            key_list = []
            key_list = copy.deepcopy(inst.keys())
            for ck in inst.coordKeys:
                key_list.append(ck)
        else:
            #
            # Filter out non-existent keys:
            #
            delkey = []
            for ik, k in enumerate(key_list):
                if (not k in inst.keys()) and (not k in inst.coordKeys):
                    delkey.append(ik)

            for ik in delkey[::-1]:
                del key_list[ik]

        #
        # Determin shape and coordinates of resulting table
        #
        coordKeys = []
        coords = {}
        needs_interpolation = False

        for ck in inst.coordKeys:
            if ck in coord_treatment.keys():
                #
                # This key is present
                #
                if coord_treatment[ck]["type"] == "all":
                    coordKeys.append(ck)
                    coords[ck] = copy.deepcopy(inst.coords[ck])

                if coord_treatment[ck]["type"] in ["const", "constUnscaled"]:
                    needs_interpolation = True
            else:
                #
                # Not present, take original
                #
                coordKeys.append(ck)
                coords[ck] = copy.deepcopy(inst.coords[ck])

        #
        # Determine shape
        #
        shape = []
        if len(coordKeys) > 0:
            for ck in coordKeys:
                shape.append(len(coords[ck]))
        else:
            #
            # Dummy array to do the interpolation anyway
            #
            coordKeys = ["dummy"]
            coords[coordKeys[0]] = [0.0]
            shape = [1]

        #
        # Initialize data
        #
        data = {}
        for k in key_list:
            data[k] = np.empty(shape)

        if needs_interpolation:
            #
            # New table does not perfectly coindice with existing coordinates, thus the data needs to be interpolated
            #
            flat_control = {}
            for ick, ck in enumerate(inst.coordKeys):
                flat_control[ck] = np.empty(shape)

            #
            # Fill control
            #
            unscaledVar = None
            for ick, ck in enumerate(inst.coordKeys):
                if ck in coord_treatment.keys():
                    #
                    # This key is present
                    #
                    if coord_treatment[ck]["type"] == "all":
                        for i in range(np.prod(shape)):
                            #
                            # Indeces in new table
                            #
                            ind = i2inds(i, shape)

                            #
                            # Specific index in full table
                            #
                            ind_full = ind[coordKeys.index(ck)]

                            #
                            # Fill control variables with corresponding coordiante values
                            #
                            flat_control[ck][ind] = inst.coords[ck][ind_full]

                    elif (
                        coord_treatment[ck]["type"] == "const"
                        or coord_treatment[ck]["type"] == "constUnscaled"
                    ):
                        #
                        # Set a single value
                        #
                        flat_control[ck] = np.ones(shape) * coord_treatment[ck]["val"]

                    elif coord_treatment[ck]["type"] == "ind":
                        #
                        # Set a single index
                        #
                        flat_control[ck] = (
                            np.ones(shape) * inst.coords[ck][coord_treatment[ck]["ind"]]
                        )

                    #
                    # Interpolate along unscaled control variable
                    #
                    if coord_treatment[ck]["type"] == "constUnscaled":
                        unscaledVar = {ck: coord_treatment[ck]["unscaledKey"]}

                else:
                    #
                    # Not present, take original
                    #
                    for i in range(np.prod(shape)):
                        #
                        # Indeces in new table
                        #
                        ind = i2inds(i, shape)

                        #
                        # Specific index in full table
                        #
                        ind_full = ind[coordKeys.index(ck)]

                        #
                        # Fill control variables with corresponding coordiante values
                        #
                        flat_control[ck][ind] = inst.coords[ck][ind_full]

            #
            # Assemble control variable for interpolation
            #
            control = np.resize(flat_control[inst.coordKeys[0]], tuple(shape + [1]))
            if len(inst.coordKeys) > 1:
                for ck in inst.coordKeys[1:]:
                    control = np.append(
                        control,
                        np.resize(flat_control[ck], tuple(shape + [1])),
                        axis=len(coordKeys),
                    )

            #
            # Interpolate
            #
            data = inst.interpolateArray(
                control,
                key_list=key_list,
                verbose=verbose,
                unscaledVar=unscaledVar,
                allowExtrapolate=allowExtrapolate,
            )

        else:
            #
            # No interpolation needed
            # Slicing is possible
            #
            flattened_indeces, shape = inst.getSliceIndeces(coord_treatment)

            #
            # Get data
            #
            for k in key_list:
                if k in inst.coordKeys:
                    #
                    # It is a coordinate
                    #
                    for i in range(np.prod(shape)):
                        #
                        # Indeces in new table
                        #
                        ind = i2inds(i, shape)
                        ind_full = i2inds(flattened_indeces[i], inst.shape)
                        data[k][ind] = inst.coords[k][ind_full[inst.coordKeys.index(k)]]
                else:
                    #
                    # It is data
                    #
                    data[k] = inst.data[k].flatten()[flattened_indeces].reshape(shape)

        #
        # Create and return subtable
        #
        ret = cls(
            dic=data,
            keys=key_list,
            coordKeys=coordKeys,
            coords=coords,
            typ=typ,
        )

        #
        # Output
        #
        if verbose:
            print("Done creating sub-table")
            sys.stdout.flush()

        return ret

    def rediscretizeAlongCoordinate(self, ck, newCoord=None):
        """
        If `self.data` contains an array for `ck`
        and `ck` is in `self.coordKeys`,
        then resample along this given coordinate
        using the values form `self.data`.
        The array in `self.data[ck]` may differ from `self.coords[ck]`.

        Parameters
        ----------
        ck: str
            Coordinate key, along wich the resampling should be executed.
        newCoord: np.ndarray
            Optional new discretization for `ck`. If not given, then the old discretization of `self.coords` is retained.
        """

        #
        # Exit if irrelevant input is given
        #
        if not ck in self.coordKeys:
            print("Key {} is not among original table coordinates.".format(ck))
            sys.stdout.flush()
            return

        #
        # Exit if coordinate is not in `self.data`
        #
        if not ck in self.data.keys():
            print("Key {} is not in table data.".format(ck))
            sys.stdout.flush()
            return

        #
        # By default use the old coordiante
        #
        if newCoord is None:
            newCoord = copy.deepcopy(self.coords[ck])
        nNew = len(newCoord)

        ############################
        # Initialize new structure #
        ############################
        #
        # Index of resampled coordinate
        #
        ick = self.coordKeys.index(ck)

        #
        # New table shape
        #
        newShape = copy.deepcopy(list(self.shape))
        newShape[ick] = len(newCoord)
        newShape = tuple(newShape)

        #
        # New data
        #
        newData = {}
        for k in self.keys():
            newData[k] = np.empty(newShape)

        #
        # Shape assiciated to the rest of the table dimensions
        # that are not being resampled
        #
        otherShape = copy.deepcopy(list(newShape))
        del otherShape[ick]
        otherShape = tuple(otherShape)

        #
        # Initialize coord treatment for slicing
        #
        coord_treatment = {}
        for k in self.coordKeys:
            if k != ck:
                coord_treatment[k] = {}
                coord_treatment[k]["type"] = "ind"
                coord_treatment[k]["ind"] = 0

        ###############
        # Interpolate #
        ###############
        #
        # Loop through other coordinates
        #
        if len(list(otherShape)) > 0:
            loc_otherShape = otherShape
            thereAreOthers = True
        else:
            loc_otherShape = [1]
            thereAreOthers = False

        for jj in range(np.prod(loc_otherShape)):
            ind_other = i2inds(jj, loc_otherShape)

            #
            # Get coordinate treatment for slicing
            #
            n = 0
            for k in self.coordKeys:
                if k != ck:
                    coord_treatment[k]["ind"] = ind_other[n]
                    n += 1

            #
            # Get slice indeces
            #
            flattened_indeces, shape1D = self.getSliceIndeces(coord_treatment)

            #
            # Get coordiante
            #
            oldCoord = self.data[ck].flatten()[flattened_indeces].reshape(shape1D)

            #
            # Re-interpolate
            #
            correctOrder = np.argsort(oldCoord)
            if thereAreOthers:
                indNew = copy.deepcopy(list(ind_other))
            else:
                indNew = []
            indNew.insert(ick, 0)
            for k in self.keys():
                #
                # Get data
                #
                localOldData = (
                    self.data[k].flatten()[flattened_indeces].reshape(shape1D)
                )

                #
                # Interpolate on new grid
                #
                localNewData = np.interp(
                    newCoord, oldCoord[correctOrder], localOldData[correctOrder]
                )

                #
                # Save in new data
                #
                for ii in range(nNew):
                    indNew[ick] = ii
                    indNew = tuple(indNew)

                    newData[k][indNew] = localNewData[ii]

                    indNew = list(indNew)

        ##############################
        # Modify data of this object #
        ##############################
        self.data = newData
        self.coords[ck] = newCoord
        self.shape = newShape
