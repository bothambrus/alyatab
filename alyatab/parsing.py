#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import re


def stripCharactersFromString(string, stripCharacterList):
    for char in stripCharacterList:
        string = re.sub(char, "", string)
    return string


def searchExpression(
    f,
    expr,
    position,
    stripNonAlphaNum,
    returnToTop,
    terminationKeyword=None,
    caseSensitive=True,
    stripCharacterList=[],
    stripComment=[],
):
    line, nline = searchExpressionLine(
        f,
        expr,
        position,
        stripNonAlphaNum,
        returnToTop,
        terminationKeyword,
        caseSensitive,
        stripCharacterList,
        stripComment,
    )
    try:
        return line.split(), nline
    except:
        print(UserWarning("Expression: {} is not found.".format(expr)))
        return None, None


def searchExpressionLine(
    f,
    expr,
    position,
    stripNonAlphaNum,
    returnToTop,
    terminationKeyword=None,
    caseSensitive=True,
    stripCharacterList=[],
    stripComment=[],
):
    found = False
    if returnToTop:
        f.seek(0)
    line = True

    #
    # Count number of lines to read
    #
    nline = 0
    while line:
        line = f.readline()
        nline += 1

        if len(line) > 0:
            line = stripCharactersFromString(line, stripComment)

        data = line.split()

        #
        # Check if we are going out of section
        #
        if len(data) > 0:
            target = stripCharactersFromString(data[0], stripCharacterList)

            if caseSensitive and target == terminationKeyword:
                break
            try:
                if (not caseSensitive) and target.lower() == terminationKeyword.lower():
                    break
            except:
                pass

        #
        # Check if expression is present in required position
        #
        if len(data) > position:
            target = data[position]
            if stripNonAlphaNum:
                target = re.sub(r"\W+", "", target)

            target = stripCharactersFromString(target, stripCharacterList)

            if caseSensitive:
                if expr == target:
                    found = True
                    break
            else:
                if expr.lower() == target.lower():
                    found = True
                    break

    if found:
        return line, nline
    return None, None
