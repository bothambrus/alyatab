#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import copy
import sys

import numpy as np
from matplotlib import colors

from alyatab.index import i2inds
from alyatab.integtable import IntegratingAlyaTable
from alyatab.interactive_plotting import getGraphObjects
from alyatab.plotting import (
    addColorbarAsAxis,
    adjustMarginToFitLabels,
    colfun,
    formatAxisTicks,
    getColormap,
    getLatexKeyAndUnit,
    getStyles,
    setPltBackend,
)


class PlottingAlyaTable(IntegratingAlyaTable):
    """
    Class to hold tabulated data for Alya with I/O capabilities.
    """

    def __init__(
        self, dic={}, keys=[], coordKeys=[], coords={}, typ="single", overwrite=True
    ):
        #
        # Initialize parent class
        #
        IntegratingAlyaTable.__init__(
            self,
            dic=dic,
            keys=keys,
            coordKeys=coordKeys,
            coords=coords,
            typ=typ,
            overwrite=overwrite,
        )

    def plot1D(
        self,
        paramKey1,
        xKey,
        yKey,
        coord_treatment=None,
        ax=None,
        fig=None,
        verbose=False,
        allowExtrapolate=True,
        addColorbar=True,
    ):
        """Plot 1D slices of this table.

        Parameters
        ----------
        paramKey1: str
            The parameter for looking up values.
        xKey: str
            The key of the horizontal coordinate.
        yKey: str
            The key of the vertical coordinate.
        coord_treatment: dict
            Dictionary determining the behaviour of other coordinates of the table.
            See: get_subtable for usage.
        ax: matplotlib.pyplot.axis
            Optional axis to plot into.
        fig: matplotlib.pyplot.figure
            Optional figure to plot into.
        verbose: bool
            Supress output of this function.

        Returns
        -------
        matplotlib.pyplot.figure
            The figure where the plots are executed.
        matplotlib.pyplot.axis
            The axis where the plots are executed.

        Notes
        -----
        `xKey` and `yKey` may be keys of the data stored in the table, or keys of the coordinates.
        """

        ############
        # GET DATA #
        ############
        #
        # Clean coord_treatment
        #
        if coord_treatment is None:
            coord_treatment_loc = {}
        else:
            coord_treatment_loc = copy.deepcopy(coord_treatment)

        #
        # Overwrite treatment of paramKey1
        # To get all possible data in this direction.
        #
        coord_treatment_loc[paramKey1] = dict(type="all")

        #
        # Get data
        #
        subtab = PlottingAlyaTable.get_subtable(
            self,
            coord_treatment=coord_treatment_loc,
            verbose=verbose,
            key_list=list(np.unique([paramKey1, xKey, yKey])),
            allowExtrapolate=allowExtrapolate,
        )
        if verbose:
            print("Extracted data along {}".format(paramKey1))
            sys.stdout.flush()

        ########
        # PLOT #
        ########
        if ax is None and fig is None:
            #
            # Create figure
            #
            try:
                plt = setPltBackend()
                fig, (ax) = plt.subplots(1, 1)
                font_style, default_line_style = getStyles()
                fig.set_size_inches(6, 4)
                fig.subplots_adjust(top=0.95, right=0.96, bottom=0.13, left=0.13)

            except Exception as e:
                print("Cannot plot, {}".format(e))
                return
        else:
            plt = setPltBackend()
            font_style, default_line_style = getStyles()

        #
        # Count number of lines
        #
        shape_noparam = []
        for key in subtab.coordKeys:
            if key != paramKey1:
                shape_noparam.append(len(subtab.coords[key]))
            else:
                shape_noparam.append(1)

        if verbose:
            print(
                "Number of lines for different parameters {} ({})".format(
                    np.product(shape_noparam), shape_noparam
                )
            )
            sys.stdout.flush()

        #
        # Line styles
        #
        line_styles = []
        nlines = max(2, np.product(shape_noparam))
        for i in range(np.product(shape_noparam)):
            line_styles.append(copy.deepcopy(default_line_style))
            line_styles[-1]["color"] = colfun(float(i) / float(nlines - 1))

        #
        # Plotting into axis
        #
        for i in range(np.product(shape_noparam)):
            #
            # Extract data of this particular line
            #
            if np.product(shape_noparam) == 1:
                x = subtab.data[xKey]
                y = subtab.data[yKey]
            else:
                ind = list(i2inds(i, shape_noparam))

                x = np.empty_like(subtab.coords[paramKey1])
                y = np.empty_like(subtab.coords[paramKey1])

                for j in range(len(subtab.coords[paramKey1])):
                    ind[subtab.coordKeys.index(paramKey1)] = j
                    x[j] = subtab.data[xKey][tuple(ind)]
                    y[j] = subtab.data[yKey][tuple(ind)]

            ax.plot(x, y, **line_styles[i])

        #
        # Add colorbar to parametric plot
        #
        if np.product(shape_noparam) != 1:
            #
            # Choose coloring variable
            #
            if np.product(shape_noparam) > np.max(shape_noparam):
                #
                # Multiple parameters
                #
                cmin = 1
                cmax = np.product(shape_noparam)
                cKeyLatex = "index"
                cUnit = "-"
            else:
                #
                # Only one parameter
                #
                maxInd = np.max(shape_noparam)
                ii = shape_noparam.index(maxInd)

                cKey = subtab.coordKeys[ii]

                cmin = np.min(subtab.coords[cKey])
                cmax = np.max(subtab.coords[cKey])
                cKeyLatex, cUnit = getLatexKeyAndUnit("prop", cKey)

            #
            # Colorbar
            #
            if addColorbar:
                addColorbarAsAxis(
                    fig,
                    cmin,
                    cmax,
                    barlabel="${} \ \mathrm{{[{}]}}$".format(cKeyLatex, cUnit),
                )

        ###############
        # FORMAT PLOT #
        ###############
        #
        # Ticks
        #
        formatAxisTicks(ax, font_style)

        #
        # Labels
        #
        xKeyLatex, xUnit = getLatexKeyAndUnit("prop", xKey)
        yKeyLatex, yUnit = getLatexKeyAndUnit("prop", yKey)

        ax.set_xlabel(
            "${} \ \mathrm{{[{}]}}$".format(xKeyLatex, xUnit), fontdict=font_style
        )
        ax.set_ylabel(
            "${} \ \mathrm{{[{}]}}$".format(yKeyLatex, yUnit), fontdict=font_style
        )

        #
        # Margin
        #
        adjustMarginToFitLabels(fig, ax)

        return fig, ax

    def plot2D(
        self,
        paramKey1,
        paramKey2,
        xKey,
        yKey,
        zKey,
        coord_treatment=None,
        plot_type="contourf",
        n_isoline=30,
        ax=None,
        fig=None,
        verbose=False,
        allowExtrapolate=True,
        addColorbar=True,
        cmin=None,
        cmax=None,
        cscale="linear",
        mapname="viridis",
    ):
        """Plot 2D slices of this table.

        Parameters
        ----------
        paramKey1: str
            First parameter for looking up values.
        paramKey2: str
            Second parameter for looking up values.
        xKey: str
            The key of the first coordinate.
        yKey: str
            The key of the second coordinate.
        zKey: str
            The key of the dependent variable.
        coord_treatment: dict
            Dictionary determining the behaviour of other coordinates of the table.
            See: get_subtable for usage.
        plot_type: str
            Determine the way of representing the 2D data:
            `contourf`:     filled contour plot
            `contour`:      countour lines only
            `plot_surface`: 3D plot representing `zKey` by height
        n_isoline: int or list
            If int:  number of isolynes in contour plots.
            If list: defined locations of contour line defined locations of contour liness
        ax: matplotlib.pyplot.axis
            Optional axis to plot into.
        fig: matplotlib.pyplot.figure
            Optional figure to plot into.
        verbose: bool
            Supress output of this function.

        Returns
        -------
        matplotlib.pyplot.figure
            The figure where the plots are executed.
        matplotlib.pyplot.axis
            The axis where the plots are executed.

        Notes
        -----
        `xKey`, `yKey` and `zKey` may be keys of the data stored in the table, or keys of the coordinates.
        """
        ############
        # GET DATA #
        ############
        #
        # Clean coord_treatment
        #
        if coord_treatment is None:
            coord_treatment_loc = {}
        else:
            coord_treatment_loc = copy.deepcopy(coord_treatment)

        #
        # Overwrite treatment of paramKey1
        # To get all possible data in this direction.
        #
        coord_treatment_loc[paramKey1] = dict(type="all")
        coord_treatment_loc[paramKey2] = dict(type="all")

        #
        # Get data
        #
        subtab = PlottingAlyaTable.get_subtable(
            self,
            coord_treatment=coord_treatment_loc,
            verbose=verbose,
            key_list=list(np.unique([paramKey1, paramKey2, xKey, yKey, zKey])),
            allowExtrapolate=allowExtrapolate,
        )
        if verbose:
            print("Extracted data along {} and {}".format(paramKey1, paramKey2))
            sys.stdout.flush()

        ########
        # PLOT #
        ########
        if ax is None and fig is None:
            #
            # Create figure
            #
            try:
                plt = setPltBackend()
                if plot_type == "plot_surface":
                    fig = plt.figure()
                    ax = fig.add_subplot(1, 1, 1, projection="3d")
                else:
                    fig, (ax) = plt.subplots(1, 1)
                font_style, default_line_style = getStyles()
                fig.set_size_inches(6, 4)
                fig.subplots_adjust(top=0.95, right=0.96, bottom=0.13, left=0.13)

            except Exception as e:
                print("Cannot plot, {}".format(e))
                return
        else:
            plt = setPltBackend()
            font_style, default_line_style = getStyles()

        #
        # Count number of layers
        #
        shape_noparam = []
        for key in subtab.coordKeys:
            if key != paramKey1 and key != paramKey2:
                shape_noparam.append(len(subtab.coords[key]))
            else:
                shape_noparam.append(1)

        if verbose:
            print(
                "Number of layers for different parameters {} ({})".format(
                    np.product(shape_noparam), shape_noparam
                )
            )
            sys.stdout.flush()

        #
        # z limits
        #
        if cmin is None:
            zmin = np.min(subtab.data[zKey])
        else:
            zmin = cmin

        if cmax is None:
            zmax = np.max(subtab.data[zKey])
        else:
            zmax = cmax

        #
        # Ensure only positive range if logarithmic scale is requested
        #
        if cscale == "log" and zmin < 0:
            zmin = zmax * 1e-6

        #
        # Create color normalized scales
        #
        if cscale == "log":
            norm = colors.LogNorm(zmin, zmax)
        else:
            norm = plt.Normalize(zmin, zmax)

        #
        # Number of levels in colorbars
        #
        if isinstance(n_isoline, int):
            nlevel = n_isoline
        elif isinstance(n_isoline, list):
            nlevel = max(3, len(n_isoline))

        #
        # Plotting into axis
        #
        for i in range(np.product(shape_noparam)):
            #
            # Extract data of this particular line
            #
            if np.product(shape_noparam) == 1:
                x = subtab.data[xKey]
                y = subtab.data[yKey]
                z = subtab.data[zKey]
            else:
                ind = list(i2inds(i, shape_noparam))

                len_p1 = len(subtab.coords[paramKey1])
                len_p2 = len(subtab.coords[paramKey2])
                x = np.empty([len_p1, len_p2])
                y = np.empty([len_p1, len_p2])
                z = np.empty([len_p1, len_p2])

                for j in range(len_p1):
                    for k in range(len_p2):
                        ind[subtab.coordKeys.index(paramKey1)] = j
                        ind[subtab.coordKeys.index(paramKey2)] = k
                        x[j, k] = subtab.data[xKey][tuple(ind)]
                        y[j, k] = subtab.data[yKey][tuple(ind)]
                        z[j, k] = subtab.data[zKey][tuple(ind)]

            if plot_type == "contourf":
                ax.contourf(
                    x,
                    y,
                    z,
                    n_isoline,
                    linewidths=0,
                    antialiased=False,
                    cmap=getColormap(name=mapname, nlevel=nlevel),
                    norm=norm,
                )
            elif plot_type == "contour":
                ax.contour(
                    x,
                    y,
                    z,
                    n_isoline,
                    linewidths=0.7,
                    antialiased=False,
                    cmap=getColormap(name=mapname, nlevel=nlevel),
                    norm=norm,
                )
            elif plot_type == "plot_surface":
                ax.plot_surface(
                    x,
                    y,
                    z,
                    linewidth=0,
                    antialiased=False,
                    cmap=getColormap(name=mapname, nlevel=nlevel),
                    norm=norm,
                )

        #
        # Colorbar
        #
        zKeyLatex, zUnit = getLatexKeyAndUnit("prop", zKey)
        if addColorbar:
            addColorbarAsAxis(
                fig,
                zmin,
                zmax,
                barlabel="${} \ \mathrm{{[{}]}}$".format(zKeyLatex, zUnit),
                mapname=mapname,
                mapnlevel=nlevel,
                cscale=cscale,
            )

        ###############
        # FORMAT PLOT #
        ###############
        #
        # Ticks
        #
        formatAxisTicks(ax, font_style)

        #
        # Labels
        #
        xKeyLatex, xUnit = getLatexKeyAndUnit("prop", xKey)
        yKeyLatex, yUnit = getLatexKeyAndUnit("prop", yKey)

        ax.set_xlabel(
            "${} \ \mathrm{{[{}]}}$".format(xKeyLatex, xUnit), fontdict=font_style
        )
        ax.set_ylabel(
            "${} \ \mathrm{{[{}]}}$".format(yKeyLatex, yUnit), fontdict=font_style
        )
        if plot_type == "plot_surface":
            ax.set_zlabel(
                "${} \ \mathrm{{[{}]}}$".format(zKeyLatex, zUnit), fontdict=font_style
            )

        #
        # Margin
        #
        adjustMarginToFitLabels(fig, ax)

        return fig, ax

    def plot2D_interactive(
        self,
        paramKey1,
        paramKey2,
        coord_treatment=None,
        verbose=False,
        allowExtrapolate=True,
    ):
        """Interactive 2D plot.


        Parameters
        ----------
        paramKey1: str
            The key of the first coordinate.
        paramKey2: str
            The key of the second coordinate.
        coord_treatment: dict
            Dictionary determining the behaviour of other coordinates of the table.
            See: get_subtable for usage.
        verbose: bool
            Supress output of this function.

        Returns
        -------
        plotly.graph_objects.figure
            The figure where the plots are executed.

        Notes
        -----
        `xKey` and `yKey` must be keys of the coordinates.
        """

        ############
        # GET DATA #
        ############
        #
        # Clean coord_treatment
        #
        if coord_treatment is None:
            coord_treatment_loc = {}
        else:
            coord_treatment_loc = copy.deepcopy(coord_treatment)

        #
        # Overwrite treatment of paramKey1
        # To get all possible data in this direction.
        #
        coord_treatment_loc[paramKey1] = dict(type="all")
        coord_treatment_loc[paramKey2] = dict(type="all")

        #
        # Get data
        #
        subtab = PlottingAlyaTable.get_subtable(
            self,
            coord_treatment=coord_treatment_loc,
            verbose=verbose,
            allowExtrapolate=allowExtrapolate,
        )
        if verbose:
            print("Extracted data along {} and {}".format(paramKey1, paramKey2))
            sys.stdout.flush()

        #
        # Count number of layers
        #
        shape_noparam = []
        for key in subtab.coordKeys:
            if key != paramKey1 and key != paramKey2:
                shape_noparam.append(len(subtab.coords[key]))
            else:
                shape_noparam.append(1)

        if verbose:
            print(
                "Number of layers for different parameters {} ({})".format(
                    np.product(shape_noparam), shape_noparam
                )
            )
            sys.stdout.flush()

        #
        # Current keys
        #
        xKeyLatex, xUnit = getLatexKeyAndUnit("prop", paramKey1)
        yKeyLatex, yUnit = getLatexKeyAndUnit("prop", paramKey2)

        ########
        # PLOT #
        ########
        go = getGraphObjects()
        fig = go.Figure()

        #
        # Update plot sizing
        #
        fig.update_layout(
            autosize=True,
            margin=dict(t=20, b=20, l=20, r=20),
            template="plotly_white",
        )

        #
        # Compile buttons to select data
        #
        allDataDicts = []
        keys_to_put = subtab.keys()
        keys_to_put = list(np.unique(keys_to_put))

        #
        # Remove coordinates from displayed keys
        #
        for ck in subtab.coordKeys:
            if ck in keys_to_put:
                del keys_to_put[keys_to_put.index(ck)]

        for ik, k in enumerate(keys_to_put):
            #
            # Global limits
            #
            kmax = np.max(subtab.data[k])
            kmin = np.min(subtab.data[k])

            for i in range(np.product(shape_noparam)):
                #
                # Get data
                #
                addition = ""
                if np.product(shape_noparam) == 1:
                    z = subtab.data[k]
                    addition = ""

                    #
                    # Allign data correctly
                    #
                    if subtab.coordKeys.index(paramKey1) > subtab.coordKeys.index(
                        paramKey2
                    ):
                        z = z
                    else:
                        z = np.transpose(z)

                else:
                    ind = list(i2inds(i, shape_noparam))

                    len_p1 = len(subtab.coords[paramKey1])
                    len_p2 = len(subtab.coords[paramKey2])
                    z = np.empty([len_p1, len_p2])

                    for l1 in range(len_p1):
                        for l2 in range(len_p2):
                            ind[subtab.coordKeys.index(paramKey1)] = l1
                            ind[subtab.coordKeys.index(paramKey2)] = l2
                            z[l1, l2] = subtab.data[k][tuple(ind)]

                    addition = ""
                    for ick, ck in enumerate(subtab.coordKeys):
                        if (not ck == paramKey1) and (not ck == paramKey2):
                            addition += " {}={:.4f}".format(
                                ck, subtab.coords[ck][ind[ick]]
                            )

                    #
                    # Allign data correctly
                    #
                    z = np.transpose(z)

                #
                # Add surface trace
                #
                fig.add_trace(
                    go.Contour(
                        x=subtab.coords[paramKey1],
                        y=subtab.coords[paramKey2],
                        z=z,
                        colorscale="Viridis",
                        visible=(ik == 0 and i == 0),
                        contours=dict(
                            start=kmin,
                            end=kmax,
                        ),
                    )
                )

                #
                # Track visibility for dropdown list
                #
                this_visible = []
                for n in range(len(keys_to_put) * np.product(shape_noparam)):
                    this_visible.append(False)
                this_visible[
                    keys_to_put.index(k) * np.product(shape_noparam) + i
                ] = True

                allDataDicts.append(
                    dict(
                        args=[{"visible": this_visible}],
                        label="{}{}".format(k, addition),
                        method="update",
                    )
                )

        #
        # Add dropdown
        #
        fig.update_layout(
            updatemenus=[
                dict(
                    buttons=list(allDataDicts),
                    direction="down",
                    pad={"r": 10, "t": 10},
                    showactive=True,
                    x=0.1,
                    xanchor="left",
                    y=1.1,
                    yanchor="top",
                ),
                dict(
                    buttons=list(
                        [
                            dict(
                                args=["type", "contour"],
                                label="Contour",
                                method="restyle",
                            ),
                            dict(
                                args=["type", "surface"],
                                label="3D Surface",
                                method="restyle",
                            ),
                            dict(
                                args=["type", "heatmap"],
                                label="Heatmap",
                                method="restyle",
                            ),
                        ]
                    ),
                    direction="down",
                    pad={"r": 10, "t": 10},
                    showactive=True,
                    x=0.37,
                    xanchor="left",
                    y=1.1,
                    yanchor="top",
                ),
            ]
        )

        #
        # Format figure
        #
        fig["layout"]["xaxis1"].update(title="{} [{}]".format(paramKey1, xUnit))
        fig["layout"]["yaxis1"].update(title="{} [{}]".format(paramKey2, yUnit))
        fig["layout"]["scene"]["xaxis"].update(title="{}".format(paramKey1))
        fig["layout"]["scene"]["yaxis"].update(title="{}".format(paramKey2))
        fig["layout"]["scene"]["zaxis"].update(title="")

        return fig
