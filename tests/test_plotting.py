#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import shutil
import numpy as np
from .context import alyatab, getTestPath, getTestTmpPath
from alyatab.plotting import (
    addColorbarAsAxis,
    setPltBackend,
    colfun,
    getStyles,
    getLatexKeyAndUnit,
)

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_plotting_setBackend():
    """
    Test setting the matplotlib backend.
    """
    plt = setPltBackend("ps")
    import matplotlib

    assert matplotlib.get_backend() == "ps"


def test_plotting_colfun():
    """
    Test getting a color from a colormap.
    """
    #
    # Test viridis
    #
    col = colfun(0)
    assert col == (0.267004, 0.004874, 0.329415, 1.0)

    col = colfun(0.5)
    assert col == (0.127568, 0.566949, 0.550556, 1.0)

    #
    # Same color if only few levels are used
    #
    col = colfun(0.5, name="jet", nlevel=5)
    assert col == (0.4838709677419355, 1.0, 0.4838709677419356, 1.0)
    col = colfun(0.55, name="jet", nlevel=5)
    assert col == (0.4838709677419355, 1.0, 0.4838709677419356, 1.0)


def test_plotting_styles():
    """
    Test font and line styles.
    """

    font_style, default_line_style = getStyles()

    assert font_style == dict(family="Sans", color="black", weight="normal", size=12)
    assert default_line_style == dict(
        marker=".",
        linestyle="-",
        fillstyle="none",
        color="black",
        linewidth=1.0,
        markersize=0,
    )


def test_plotting_getLatexKeyAndUnit():
    """
    Test labels.
    """

    type = "prop"
    assert getLatexKeyAndUnit(type, "gazorp") == ("gazorp", "?")
    assert getLatexKeyAndUnit(type, "Z") == ("Z", "-")
    assert getLatexKeyAndUnit(type, "ZMEAN") == ("\widetilde{{Z}}", "-")
    assert getLatexKeyAndUnit(type, "ZVAR") == ("Z_v", "-")
    assert getLatexKeyAndUnit(type, "x") == ("x", "m")
    assert getLatexKeyAndUnit(type, "C") == ("C", "-")
    assert getLatexKeyAndUnit(type, "CMEAN") == ("\widetilde{{C}}", "-")
    assert getLatexKeyAndUnit(type, "CVAR") == ("C_v", "-")
    assert getLatexKeyAndUnit(type, "I") == ("i", "-")
    assert getLatexKeyAndUnit(type, "IMEAN") == ("\widetilde{{i}}", "-")
    assert getLatexKeyAndUnit(type, "EqRat") == ("\phi", "-")
    assert getLatexKeyAndUnit(type, "Lambda") == ("\lambda", "-")
    assert getLatexKeyAndUnit(type, "Yc") == ("Y_c", "-")
    assert getLatexKeyAndUnit(type, "chi") == ("\chi", "1/s")
    assert getLatexKeyAndUnit(type, "Dt") == ("D_t", "m^2/s")
    assert getLatexKeyAndUnit(type, "Dfuel") == ("D_{{fuel}}", "m^2/s")
    assert getLatexKeyAndUnit(type, "HeatRel") == ("\dot{{\omega}}_{{T}}", "J/(m^3s)")
    assert getLatexKeyAndUnit(type, "omegaYc") == (
        "\dot{{\omega}}_{{Y_c}}",
        "kg/(m^3s)",
    )
    assert getLatexKeyAndUnit(type, "omegaC") == ("\dot{{\omega}}_{{C}}", "kg/(m^3s)")
    assert getLatexKeyAndUnit(type, "nativeStrain") == ("a", "1/s")
    assert getLatexKeyAndUnit(type, "zgrad") == ("\\nabla Z", "1/m")
    assert getLatexKeyAndUnit(type, "cgrad") == ("\\nabla C", "1/m")
    assert getLatexKeyAndUnit(type, "T") == ("T", "K")
    assert getLatexKeyAndUnit(type, "overlap") == ("overlap", "-")
    assert getLatexKeyAndUnit(type, "Yk_CO2") == ("Y_{CO2}", "-")
    assert getLatexKeyAndUnit(type, "omegak_CO2") == ("\dot{\omega}_{CO2}", "kg/(m^3s)")

    assert getLatexKeyAndUnit("speci", "CO2") == ("Y_{CO2}", "-")
    assert getLatexKeyAndUnit("source", "CO2") == ("\dot{\omega}_{CO2}", "kg/(m^3s)")


def testAddColorbarAsAxis():
    """
    Test adding different colorbar locations
    """

    case_path = os.path.join(tmp_test_path, "add_different_colorbars")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    #
    # Make figure
    #
    plt = setPltBackend("ps")
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    #
    # Add normal colorbars
    #
    addColorbarAsAxis(
        fig, 1, 2, barx=0.85, bary=0.2, bardx=0.01, bardy=0.6, barlabel="a"
    )
    addColorbarAsAxis(
        fig, 2, 3, barx=0.15, bary=0.2, bardx=0.02, bardy=0.6, barlabel="b"
    )
    addColorbarAsAxis(
        fig,
        3,
        4,
        barx=0.2,
        bary=0.85,
        bardx=0.6,
        bardy=0.03,
        barorientation="horizontal",
        barlabel="c",
    )
    addColorbarAsAxis(
        fig,
        4,
        5,
        barx=0.2,
        bary=0.15,
        bardx=0.6,
        bardy=0.04,
        barorientation="horizontal",
        barlabel="d",
    )

    addColorbarAsAxis(
        fig,
        5,
        6,
        barx=0.5,
        bary=0.5,
        bardx=0.1,
        bardy=0.1,
        barlabel="e",
        adjustmargin=False,
        mapname="Reds",
        mapnlevel=11,
    )

    #
    # Save
    #
    fig.savefig(
        os.path.join(case_path, "example_colorbars.eps"), format="eps", dpi=1200
    )
