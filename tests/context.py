#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import alyatab


def getTestPath():
    """
    Get full path of tests directory.
    """
    return os.path.abspath(os.path.dirname(__file__))


def getTestTmpPath():
    """
    Get full path of temporary test directory,
    and make it if it does not exist.
    """
    tmp_test_path = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "tmp_testing")
    )
    if not os.path.exists(tmp_test_path):
        os.makedirs(tmp_test_path)
    return tmp_test_path
