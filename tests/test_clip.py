#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import alyatab
from alyatab.clip import clipUnderAndOverflow


#
# Testing clipping
#
def test_clipping():
    """
    Test clipping of too large and too small floats.
    """
    #
    # Float
    #
    assert clipUnderAndOverflow(0) == 0
    assert clipUnderAndOverflow(1e-20) == 0
    assert clipUnderAndOverflow(-1e-20) == 0
    assert clipUnderAndOverflow(1e-16) == 1e-16
    assert clipUnderAndOverflow(-1e-16) == -1e-16
    assert clipUnderAndOverflow(1) == 1
    assert clipUnderAndOverflow(-1) == -1
    assert clipUnderAndOverflow(1e20) == 1e16
    assert clipUnderAndOverflow(-1e20) == -1e16

    assert clipUnderAndOverflow(0, snapToZero=False) == 1e-16
    assert clipUnderAndOverflow(1e-20, snapToZero=False) == 1e-16
    assert clipUnderAndOverflow(-1e-20, snapToZero=False) == -1e-16
    assert clipUnderAndOverflow(1e-16, snapToZero=False) == 1e-16
    assert clipUnderAndOverflow(-1e-16, snapToZero=False) == -1e-16
    assert clipUnderAndOverflow(1, snapToZero=False) == 1
    assert clipUnderAndOverflow(-1, snapToZero=False) == -1
    assert clipUnderAndOverflow(1e20, snapToZero=False) == 1e16
    assert clipUnderAndOverflow(-1e20, snapToZero=False) == -1e16

    #
    # Array
    #
    arr = np.array([0, 1e-20, -1e-20, 1e-16, -1e-16, 1, -1, 1e16, -1e16, 1e20, -1e20])
    assert all(
        clipUnderAndOverflow(arr)
        == np.array([0, 0, 0, 1e-16, -1e-16, 1, -1, 1e16, -1e16, 1e16, -1e16])
    )
    assert all(
        clipUnderAndOverflow(arr, snapToZero=False)
        == np.array(
            [1e-16, 1e-16, -1e-16, 1e-16, -1e-16, 1, -1, 1e16, -1e16, 1e16, -1e16]
        )
    )
