#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import shutil
import numpy as np
from .context import alyatab, getTestPath, getTestTmpPath
from alyatab.table import AlyaTable
from alyatab.index import i2inds
from alyatab.plotting import setPltBackend
from alyatab.interactive_plotting import getGraphObjects

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


#
# Dummy shape:
#
shape = [4, 5, 6]
dims = len(shape)
len = np.prod(shape)

#
# Dummy coordinates
#
xcoord = np.linspace(0, 1, shape[0])
ycoord = np.logspace(-2, 3, shape[1])
zcoord = np.linspace(10, 20, shape[2])

#
# Dummy data
#
data = {}
data["a"] = np.reshape(np.linspace(0, 1, len), shape)
data["b"] = np.reshape(np.linspace(10, 20, len), shape)
data["c"] = np.reshape(np.linspace(1e12, 1e20, len), shape)

#
# Dummy data 2
#
data2 = {}
data2["a"] = np.reshape(np.linspace(0, 1, len), shape)
data2["b"] = np.reshape(np.linspace(10, 20, len), shape)
data2["c"] = np.reshape(np.linspace(10, 20, len), shape)
data2["d"] = np.reshape(np.linspace(10, 20, len), shape)
data2["e"] = np.reshape(np.linspace(10, 20, len), shape)
data2["f"] = np.reshape(np.linspace(10, 20, len), shape)
data2["g"] = np.reshape(np.linspace(10, 20, len), shape)
data2["h"] = np.reshape(np.linspace(10, 20, len), shape)
data2["i"] = np.reshape(np.linspace(10, 20, len), shape)
data2["j"] = np.reshape(np.linspace(10, 20, len), shape)
data2["k"] = np.reshape(np.linspace(10, 20, len), shape)
data2["l"] = np.reshape(np.linspace(10, 20, len), shape)
data2["m"] = np.reshape(np.linspace(10, 20, len), shape)
data2["n"] = np.reshape(np.linspace(10, 20, len), shape)


def test_create_AlyaTable():
    """
    Test creation of Alya table.
    """
    tab = AlyaTable(typ="single")
    tab = AlyaTable(typ="double")


def test_adding_coordinates_to_AlyaTable():
    """
    Test adding coordinates to an Alya table.
    """
    tab = AlyaTable(typ="single")

    tab.addCoordinate("x", xcoord)
    tab.addCoordinate("y", ycoord)

    sh = tab.getShape()
    assert sh == (4, 5)


def test_adding_data_to_AlyaTable():
    """
    Test adding data to an Alya table.
    """
    tab = AlyaTable(typ="single")

    tab.addCoordinate("x", xcoord)
    tab.addCoordinate("y", ycoord)

    sh = tab.getShape()
    assert sh == (4, 5)

    #
    # Add one normal array
    #
    tab.addData("a", data["a"][:, :, 0])
    assert tab.keys() == ["a"]

    #
    # Try adding same without overwriting
    #
    tab.addData("a", data["a"][:, :, 0], overwrite=False)
    assert tab.keys() == ["a"]

    #
    # Try adding non-array
    #
    tab.addData("non", "ABC")
    assert tab.keys() == ["a"]

    #
    # Try adding wrong dimensions
    #
    tab.addData("b", data["b"])
    assert tab.keys() == ["a"]

    #
    # Try adding wrong shape
    #
    tab.addData("b", data["b"][1:, :, 0])
    assert tab.keys() == ["a"]

    #
    # Add another normal array
    #
    tab.addData("b", data["b"][:, :, 1])
    assert tab.keys() == ["a", "b"]
    assert np.abs(tab.data["a"][1, 1] - 0.30252102) < 1e-5
    assert np.abs(tab.data["b"][1, 1] - 13.109243) < 1e-5

    #
    # Remove wrong key
    #
    tab.removeData("c")
    assert tab.keys() == ["a", "b"]

    #
    # Remove correct key
    #
    tab.removeData("b")
    assert tab.keys() == ["a"]


def test_fill_AlyaTable():
    """
    Test creation of Alya table from pre-existing data.
    """
    tab = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )

    assert np.abs(tab.data["a"][1, 1, 1] - 0.31092438101768494) < 1e-7
    assert tab.shape == tuple(shape)
    assert tab.keys() == ["a", "b", "c"]


def test_slicing_AlyaTable():
    """
    Test slicing an Alya table according to given criteria.
    """
    tab = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )

    #
    # Get full array
    #
    fullSlice, flattened_indeces = tab.getSlice("a", {})
    assert np.all(np.abs(tab.data["a"] - fullSlice) < 1e-5)

    slice, flattened_indeces = tab.getSlice(
        "a", {"x": {"type": "ind", "ind": 1}, "z": {"type": "ind", "ind": 2}}
    )
    print(tab.data["a"][1, :, 2])
    print(slice)

    assert np.all(
        np.abs(
            tab.data["a"][1, :, 2]
            - np.array([0.26890758, 0.31932774, 0.3697479, 0.42016807, 0.47058824])
        )
        < 1e-5
    )
    assert np.all(
        np.abs(
            slice
            - np.array([0.26890758, 0.31932774, 0.3697479, 0.42016807, 0.47058824])
        )
        < 1e-5
    )

    #
    # Try modifying the slice (Show that it is a deep copy)
    #
    slice[0] = 1.0
    print(tab.data["a"][1, :, 2])
    print(slice)
    assert np.all(
        np.abs(
            tab.data["a"][1, :, 2]
            - np.array([0.26890758, 0.31932774, 0.3697479, 0.42016807, 0.47058824])
        )
        < 1e-5
    )
    assert np.all(
        np.abs(slice - np.array([1.0, 0.31932774, 0.3697479, 0.42016807, 0.47058824]))
        < 1e-5
    )

    #
    # Test setting slice
    #
    array = [1, 2, 3, 4, 5]
    tab.setSlice(
        "a", {"x": {"type": "ind", "ind": 1}, "z": {"type": "ind", "ind": 2}}, array
    )
    print(tab.data["a"][1, :, 2])
    assert np.all(np.abs(tab.data["a"][1, :, 2] - np.array([1, 2, 3, 4, 5])) < 1e-5)

    #
    # Test wrong array size
    #
    array = [
        0,
        0,
        0,
        0,
        0,
        0,
    ]
    tab.setSlice(
        "a", {"x": {"type": "ind", "ind": 1}, "z": {"type": "ind", "ind": 2}}, array
    )
    print(tab.data["a"][1, :, 2])
    assert np.all(np.abs(tab.data["a"][1, :, 2] - np.array([1, 2, 3, 4, 5])) < 1e-5)


def test_slicing_with_index_list_AlyaTable():
    """
    Test slicing an Alya table with a list of indeces.
    """
    tab = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )

    slice, flattened_indeces = tab.getSlice(
        "a",
        {"x": {"type": "indlist", "indlist": [1, 1]}, "z": {"type": "ind", "ind": 2}},
    )
    print(tab.data["a"][1, :, 2])
    print(slice)

    assert np.all(
        np.abs(
            tab.data["a"][1, :, 2]
            - np.array([0.26890758, 0.31932774, 0.3697479, 0.42016807, 0.47058824])
        )
        < 1e-5
    )
    assert np.all(
        np.abs(
            slice
            - np.array(
                [
                    [0.26890758, 0.31932774, 0.3697479, 0.42016807, 0.47058824],
                    [0.26890758, 0.31932774, 0.3697479, 0.42016807, 0.47058824],
                ]
            )
        )
        < 1e-5
    )

    slice, flattened_indeces = tab.getSlice(
        "a",
        {"x": {"type": "indlist", "indlist": [1, 2]}, "z": {"type": "ind", "ind": 2}},
    )
    print(tab.data["a"][1, :, 2])
    print(slice)

    assert np.all(
        np.abs(
            tab.data["a"][1, :, 2]
            - np.array([0.26890758, 0.31932774, 0.3697479, 0.42016807, 0.47058824])
        )
        < 1e-5
    )
    assert np.all(
        np.abs(
            slice
            - np.array(
                [
                    [0.26890758, 0.31932774, 0.3697479, 0.42016807, 0.47058824],
                    [0.52100843, 0.5714286, 0.62184876, 0.6722689, 0.7226891],
                ]
            )
        )
        < 1e-5
    )


def test_reading_AlyaTable():
    """
    Test reading a Alya table from existing data.
    """
    tab = AlyaTable()

    orig_tab_path = os.path.join(
        test_path,
        "data",
        "example_flamelet_table",
        "example_flamelet_table.dat",
    )

    #
    # Read table
    #
    tab.readText(orig_tab_path)
    assert np.abs(tab.data["T"][11, 15] - 825.2413) < 1e-3


def test_isfloat_AlyaTable():
    """
    Test reading a Alya table from existing data.
    """
    tab = AlyaTable()
    assert tab.__isFloat__(1)
    assert tab.__isFloat__(1.1)
    assert not tab.__isFloat__("a")
    assert not tab.__isFloat__(None)


def test_writing_AlyaTable():
    """
    Test writing a Alya table to file.
    """
    tab = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )
    testTab = AlyaTable()

    case_path = os.path.join(tmp_test_path, "table_writing")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    #
    # Write table
    #
    tab_path = os.path.join(case_path, "normal.dat")
    tab.writeText(tab_path)
    testTab.readText(tab_path)

    assert np.abs(testTab.data["a"][1, 1, 1] - 0.31092438101768494) < 1e-7
    assert testTab.shape == tuple(shape)
    assert testTab.keys() == ["a", "b", "c"]

    #
    # Test low data per line
    #
    tab_path = os.path.join(case_path, "low_data_per_line.dat")
    tab.writeText(tab_path, dataPerLine=3)
    testTab.readText(tab_path)

    assert np.abs(testTab.data["a"][1, 1, 1] - 0.31092438101768494) < 1e-7
    assert testTab.shape == tuple(shape)
    assert testTab.keys() == ["a", "b", "c"]

    #
    # Reorder coordinates
    #
    tab_path = os.path.join(case_path, "reorder_coords.dat")
    tab.writeText(tab_path, coordOrder=["y", "f", "z", "x", "b"])
    testTab.readText(tab_path)

    assert np.abs(testTab.data["a"][1, 1, 1] - 0.31092438101768494) < 1e-7
    assert testTab.shape == (5, 6, 4)
    assert testTab.keys() == ["a", "b", "c"]

    #
    # Write selected keys
    #
    tab_path = os.path.join(case_path, "select_keys.dat")
    tab.writeText(tab_path, write_keys=["c", "a", "f"])
    testTab.readText(tab_path)

    assert np.abs(testTab.data["a"][1, 1, 1] - 0.31092438101768494) < 1e-7
    assert testTab.shape == tuple(shape)
    assert testTab.keys() == ["c", "a"]


def test_writing_long_AlyaTable():
    """
    Test writing a Alya table to file with more data.
    """
    tab = AlyaTable(
        dic=data2,
        keys=["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )
    testTab = AlyaTable()

    case_path = os.path.join(tmp_test_path, "table_writing_long")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    #
    # Write table
    #
    tab_path = os.path.join(case_path, "normal.dat")
    tab.writeText(tab_path)
    testTab.readText(tab_path)

    assert np.abs(testTab.data["a"][1, 1, 1] - 0.31092438101768494) < 1e-7
    assert testTab.shape == tuple(shape)
    assert testTab.keys() == [
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
    ]

    #
    # Test low data per line
    #
    tab_path = os.path.join(case_path, "low_data_per_line.dat")
    tab.writeText(
        tab_path,
        dataPerLine=5,
        commentsToColumns=dict(x="first coorinate", k="kay variable", l="ell variable"),
    )
    testTab.readText(tab_path)

    assert np.abs(testTab.data["a"][1, 1, 1] - 0.31092438101768494) < 1e-7
    assert testTab.shape == tuple(shape)
    assert testTab.keys() == [
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
    ]


def test_adding_empty_array_AlyaTable():
    """
    Test adding an empty array to an Alya table.
    """
    tab = AlyaTable(typ="single")

    tab.addCoordinate("x", xcoord)
    tab.addCoordinate("y", ycoord)

    sh = tab.getShape()
    assert sh == (4, 5)

    #
    # Test empty array
    #
    tab.addEmptyData("emp")
    assert tab.keys() == ["emp"]


def test_adding_data_corresponding_to_coordinate_AlyaTable():
    """
    Test adding an array that is the local coordinate value to an Alya table.
    """
    tab = AlyaTable(typ="single")

    tab.addCoordinate("x", xcoord)
    tab.addCoordinate("y", ycoord)

    sh = tab.getShape()
    assert sh == (4, 5)

    #
    # Test non-existent coordinate
    #
    tab.addGridOfCoordinate("z")
    assert tab.keys() == []

    #
    # Test adding coordinate
    #
    tab.addGridOfCoordinate("x")
    assert tab.keys() == ["x"]
    assert np.abs(tab.data["x"][1, 1] - 0.3333333) < 1e-5
    assert np.abs(tab.data["x"][1, 2] - 0.3333333) < 1e-5
    assert np.abs(tab.data["x"][2, 2] - 0.6666666) < 1e-5

    tab.addGridOfAllCoordinates()
    assert tab.keys() == ["x", "y"]
    assert np.abs(tab.data["x"][1, 1] - 0.3333333) < 1e-5
    assert np.abs(tab.data["x"][1, 2] - 0.3333333) < 1e-5
    assert np.abs(tab.data["x"][2, 2] - 0.6666666) < 1e-5

    assert np.abs(tab.data["y"][1, 1] - 0.17782794) < 1e-5
    assert np.abs(tab.data["y"][2, 1] - 0.17782794) < 1e-5
    assert np.abs(tab.data["y"][1, 2] - 3.1622777) < 1e-5
    assert np.abs(tab.data["y"][2, 2] - 3.1622777) < 1e-5


def test_interpolating_in_AlyaTable():
    """
    Test reading an Alya table from existing data and interpolating in it.
    """
    tab = AlyaTable()

    orig_tab_path = os.path.join(
        test_path,
        "data",
        "example_flamelet_table",
        "example_flamelet_table.dat",
    )

    #
    # Read table
    #
    tab.readText(orig_tab_path)

    z = 0.05
    c = 0.8
    data = tab.interpolate([z, c], key_list=["Z", "C", "T"])
    assert np.abs(data["Z"] - z) < 1e-6
    assert np.abs(data["C"] - c) < 1e-6
    assert np.abs(data["T"] - 1694.2757741768332) < 1e-4

    z = 0.0
    c = 0.0
    data = tab.interpolate([z, c], key_list=["Z", "C", "T"])
    assert np.abs(data["Z"] - z) < 1e-6
    assert np.abs(data["C"] - c) < 1e-6
    assert np.abs(data["T"] - 298.1499938964844) < 1e-6

    z = 1.0
    c = 1.0
    data = tab.interpolate([z, c], key_list=["Z", "C", "T"])
    assert np.abs(data["Z"] - z) < 1e-6
    assert np.abs(data["C"] - c) < 1e-6
    assert np.abs(data["T"] - 298.1500549316406) < 1e-6

    #
    # Test that extrapolation is not done (clipping)
    #
    z = 1.5
    c = 1.5
    data = tab.interpolate([z, c], key_list=["Z", "C", "T"])
    assert np.abs(data["Z"] - 1.0) < 1e-6
    assert np.abs(data["C"] - 1.0) < 1e-6
    assert np.abs(data["T"] - 298.1500549316406) < 1e-6
    z = -0.5
    c = -0.5
    data = tab.interpolate([z, c], key_list=["Z", "C", "T"])
    assert np.abs(data["Z"] - 0.0) < 1e-6
    assert np.abs(data["C"] - 0.0) < 1e-6
    assert np.abs(data["T"] - 298.1499938964844) < 1e-6

    z = -0.5
    c = -0.5
    data = tab.interpolate([z, c], key_list=["Z", "C", "T"], allowExtrapolate=False)
    assert np.isnan(data["Z"])
    assert np.isnan(data["C"])
    assert np.isnan(data["T"])

    #
    # Test interpolating based on unscaled control variable
    #
    z = 0.05
    c = 0.8
    data = tab.interpolate([z, c], key_list=["Z", "C", "T", "Yc"])
    assert np.abs(data["Z"] - z) < 1e-6
    assert np.abs(data["C"] - c) < 1e-6
    assert np.abs(data["Yc"] - 0.019074992708582154) < 1e-6
    assert np.abs(data["T"] - 1694.2757741768332) < 1e-4

    z = 0.05
    Yc = 0.019
    data = tab.interpolate(
        [z, Yc], key_list=["Z", "C", "T", "Yc"], unscaledVar={"C": "Yc"}
    )
    assert np.abs(data["Z"] - z) < 1e-6
    assert np.abs(data["C"] - 0.7968548259124395) < 1e-6
    assert np.abs(data["Yc"] - Yc) < 1e-6
    assert np.abs(data["T"] - 1689.781598308665) < 1e-3
    assert np.abs(data["extrapolated"] - 0) < 1e-6

    z = 0.05
    Yc = 0.1
    data = tab.interpolate(
        [z, Yc], key_list=["Z", "C", "T", "Yc"], unscaledVar={"C": "Yc"}
    )
    assert np.abs(data["Z"] - z) < 1e-6
    assert np.abs(data["C"] - 1.0) < 1e-6
    assert np.abs(data["Yc"] - 0.023843741490790882) < 1e-6
    assert np.abs(data["T"] - 2073.5127371920853) < 1e-3
    assert np.abs(data["extrapolated"] - 1) < 1e-6


def test_interpolating_array_in_AlyaTable():
    """
    Test reading an Alya table from existing data and interpolating an array of points in it.
    """
    tab = AlyaTable()

    orig_tab_path = os.path.join(
        test_path,
        "data",
        "example_flamelet_table",
        "example_flamelet_table.dat",
    )

    #
    # Read table
    #
    tab.readText(orig_tab_path)

    #
    # Test 0D
    #
    valarr = np.zeros([2])
    valarr[:] = [0.05, 0.75]

    data = tab.interpolateArray(valarr, key_list=["Z", "C", "T"])
    assert np.all(np.abs(data["Z"] - np.array([0.05])) < 1e-6)
    assert np.all(np.abs(data["C"] - np.array([0.75])) < 1e-6)
    assert np.all(np.abs(data["T"] - 1622.8301921) < 1e-5)

    #
    # Test 1D
    #
    valarr = np.zeros([3, 2])
    valarr[0, :] = [0.05, 0.75]
    valarr[1, :] = [0.0, 0.0]
    valarr[2, :] = [1.0, 1.0]

    goaltempe = np.zeros([3])
    goaltempe[0] = 1622.8301921
    goaltempe[1] = 298.1499938964844
    goaltempe[2] = 298.1500549316406

    data = tab.interpolateArray(valarr, key_list=["Z", "C", "T"])
    assert np.all(np.abs(data["Z"] - valarr[:, 0]) < 1e-6)
    assert np.all(np.abs(data["C"] - valarr[:, 1]) < 1e-6)
    assert np.all(np.abs(data["T"] - goaltempe) < 1e-5)

    #
    # Test 2D
    #
    valarr = np.zeros([3, 2, 2])
    valarr[0, 0, :] = [0.05, 0.75]
    valarr[1, 0, :] = [0.0, 0.0]
    valarr[2, 0, :] = [1.0, 1.0]
    valarr[0, 1, :] = [0.052, 0.78]
    valarr[1, 1, :] = [0.02, 0.02]
    valarr[2, 1, :] = [0.98, 0.98]

    goaltempe = np.zeros([3, 2])
    goaltempe[0, 0] = 1622.8301921
    goaltempe[1, 0] = 298.1499938964844
    goaltempe[2, 0] = 298.1500549316406
    goaltempe[0, 1] = 1707.25997803
    goaltempe[1, 1] = 315.4725606
    goaltempe[2, 1] = 320.29501719

    data = tab.interpolateArray(valarr, key_list=["Z", "C", "T"], verbose=True)
    assert np.all(np.abs(data["Z"] - valarr[:, :, 0]) < 1e-6)
    assert np.all(np.abs(data["C"] - valarr[:, :, 1]) < 1e-6)
    assert np.all(np.abs(data["T"] - goaltempe) < 1e-4)


def test_getting_subtable_of_AlyaTable():
    """
    Test reading an Alya table from existing data and creating a subtable of it.
    """
    tab = AlyaTable()
    tabB = AlyaTable(typ="double")

    orig_tab_path = os.path.join(
        test_path,
        "data",
        "example_flamelet_table",
        "example_flamelet_table.dat",
    )

    #
    # Read table
    #
    tab.readText(orig_tab_path)
    tabB.readText(orig_tab_path)

    #
    # Get subtable that needs interpolation
    #
    tab2 = AlyaTable.get_subtable(
        tab,
        coord_treatment={"Z": {"type": "const", "val": 0.05}, "C": {"type": "all"}},
        verbose=True,
    )
    assert np.abs(tab2.data["T"][15] - 1622.8302) < 1e-3
    assert np.abs(tab2.data["omegaYc"][15] - 33.978416) < 1e-3

    #
    # Get subtable that does NOT need interpolation
    #
    tab3 = AlyaTable.get_subtable(
        tab,
        coord_treatment={"Z": {"type": "all"}, "C": {"type": "ind", "ind": 15}},
        verbose=True,
    )
    assert np.abs(tab3.data["T"][11] - 825.2413) < 1e-3

    #
    # Get Full table
    #
    tab4 = AlyaTable.get_subtable(
        tab,
        coord_treatment={"Z": {"type": "all"}, "C": {"type": "all"}},
        verbose=True,
        typ="double",
    )
    assert np.abs(tab4.data["T"][11, 15] - 825.2413) < 1e-3
    assert isinstance(tab4.data["T"][11, 15], np.float64)
    assert "Z" in tab4.keys()
    assert "H" in tab4.keys()

    #
    # Get subtable that needs interpolation for specific keys
    #
    tab5 = AlyaTable.get_subtable(
        tab,
        coord_treatment={"Z": {"type": "const", "val": 0.05}},
        verbose=True,
        key_list=["T", "omegaYc", "forSureNotAKey"],
    )
    assert np.abs(tab5.data["T"][15] - 1622.8302) < 1e-3
    assert np.abs(tab5.data["omegaYc"][15] - 33.978416) < 1e-3
    assert not "forSureNotAKey" in tab5.keys()
    assert not "Z" in tab5.keys()
    assert not "H" in tab5.keys()

    #
    # Get Full table
    #
    tab6 = AlyaTable.get_subtable(
        tab, coord_treatment={"Z": {"type": "all"}}, verbose=True
    )
    assert np.abs(tab6.data["T"][11, 15] - 825.2413) < 1e-3
    assert isinstance(tab6.data["T"][11, 15], np.float32)

    #
    # Get dummy table that is modified in all dimensions
    #
    tab7 = AlyaTable.get_subtable(
        tab,
        coord_treatment={
            "Z": {"type": "const", "val": 0.05},
            "C": {"type": "ind", "ind": 15},
        },
        verbose=True,
    )
    assert np.abs(tab7.data["C"][0] - 0.75) < 1e-6
    assert np.abs(tab7.data["Z"][0] - 0.05) < 1e-6
    assert np.abs(tab7.data["T"][0] - 1622.8302) < 1e-3
    assert np.abs(tab7.data["omegaYc"][0] - 33.978416) < 1e-3
    assert tab7.coordKeys == ["dummy"]
    assert tab7.coords["dummy"][0] == 0

    #
    # Get subtable that needs interpolation with constant unscaled control variable
    #
    tab8 = AlyaTable.get_subtable(
        tabB,
        coord_treatment={
            "Z": {"type": "all"},
            "C": {"type": "constUnscaled", "val": 0.018, "unscaledKey": "Yc"},
        },
        verbose=True,
    )
    assert np.abs(tab8.data["T"][2] - 1624.8514) < 1e-3
    assert np.abs(tab8.data["omegaYc"][2] - 31.31409) < 1e-3


def test_1D_plot_of_AlyaTable():
    """
    Test reading an Alya table from existing data and plotting it along 1 parameter.
    """
    tab = AlyaTable()

    orig_tab_path = os.path.join(
        test_path,
        "data",
        "example_flamelet_table",
        "example_flamelet_table.dat",
    )

    #
    # Read table
    #
    tab.readText(orig_tab_path)

    #
    # Get subtable that needs interpolation
    #
    fig, ax = tab.plot1D(
        paramKey1="C",
        xKey="Yc",
        yKey="omegaYc",
        coord_treatment={"Z": {"type": "const", "val": 0.05}},
        verbose=True,
    )
    assert not fig is None
    assert not ax is None

    #
    # Get subtable that needs interpolation
    # Pass axis to script
    #
    plt = setPltBackend("ps")
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    fig, ax = tab.plot1D(
        paramKey1="C",
        xKey="Yc",
        yKey="omegaYc",
        coord_treatment={"Z": {"type": "all"}},
        verbose=True,
        fig=fig,
        ax=ax,
    )
    assert not fig is None
    assert not ax is None


def test_2D_plot_of_AlyaTable():
    """
    Test reading an Alya table from existing data and plotting it along 2 parameters.
    """
    tab = AlyaTable()

    orig_tab_path = os.path.join(
        test_path,
        "data",
        "example_flamelet_table",
        "example_flamelet_table.dat",
    )

    #
    # Read table
    #
    tab.readText(orig_tab_path)

    #
    # Get subtable that needs interpolation
    #
    fig, ax = tab.plot2D(
        paramKey1="Z",
        paramKey2="C",
        xKey="Z",
        yKey="Yk_CO2",
        zKey="omegak_CO2",
        coord_treatment={},
        n_isoline=10,
        verbose=True,
    )
    assert not fig is None
    assert not ax is None

    #
    # Get subtable that needs interpolation
    #
    fig, ax = tab.plot2D(
        paramKey1="Z",
        paramKey2="C",
        xKey="Z",
        yKey="Yk_CO2",
        zKey="omegak_CO2",
        plot_type="contour",
        n_isoline=[-0.4, -0.2, 0.0, 0.2, 0.4],
        verbose=True,
        cmin=-0.5,
        cmax=0.5,
    )
    assert not fig is None
    assert not ax is None

    #
    # Use log-scale
    #
    fig, ax = tab.plot2D(
        paramKey1="Z",
        paramKey2="C",
        xKey="Z",
        yKey="Yk_CO2",
        zKey="omegak_CO2",
        plot_type="contour",
        n_isoline=[0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1.0],
        verbose=True,
        cmin=1e-2,
        cmax=1e0,
        cscale="log",
    )
    assert not fig is None
    assert not ax is None

    #
    # Use log-scale with negative limit
    #
    fig, ax = tab.plot2D(
        paramKey1="Z",
        paramKey2="C",
        xKey="Z",
        yKey="Yk_CO2",
        zKey="omegak_CO2",
        plot_type="contour",
        n_isoline=[0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1.0],
        verbose=True,
        cmin=-1,
        cmax=1e0,
        cscale="log",
    )
    assert not fig is None
    assert not ax is None

    plt = setPltBackend("ps")
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection="3d")
    fig, ax = tab.plot2D(
        paramKey1="Z",
        paramKey2="C",
        xKey="Z",
        yKey="Yk_CO2",
        zKey="omegak_CO2",
        plot_type="plot_surface",
        verbose=True,
        fig=fig,
        ax=ax,
        mapname="Reds",
    )
    assert not fig is None
    assert not ax is None

    #
    # Parametric 3D
    #
    tab2 = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )

    #
    # One layer
    #
    fig, ax = tab2.plot2D(
        paramKey1="x",
        paramKey2="y",
        xKey="x",
        yKey="y",
        zKey="a",
        plot_type="plot_surface",
        coord_treatment={"z": {"type": "const", "val": 15}},
        verbose=True,
    )
    assert not fig is None
    assert not ax is None

    #
    # Multiple layers
    #
    fig, ax = tab2.plot2D(
        paramKey1="x",
        paramKey2="y",
        xKey="x",
        yKey="y",
        zKey="a",
        plot_type="plot_surface",
        coord_treatment={"z": {"type": "all"}},
        verbose=True,
    )
    assert not fig is None
    assert not ax is None


def test_interactive_2D_plot_of_AlyaTable():
    """
    Test reading an Alya table from existing data and plotting it along 2 parameters.
    """
    tab = AlyaTable()

    orig_tab_path = os.path.join(
        test_path,
        "data",
        "example_flamelet_table",
        "example_flamelet_table.dat",
    )

    #
    # Read table
    #
    tab.readText(orig_tab_path)

    go = getGraphObjects()

    #
    # Get subtable that needs interpolation
    #
    fig = tab.plot2D_interactive(
        paramKey1="Z",
        paramKey2="C",
        verbose=True,
    )
    assert not fig is None

    #
    # Reverse order
    #
    fig = tab.plot2D_interactive(
        paramKey1="C",
        paramKey2="Z",
        verbose=True,
    )
    assert not fig is None

    #
    # Parametric 3D
    #
    tab2 = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )

    #
    # One layer
    #
    fig = tab2.plot2D_interactive(
        paramKey1="x",
        paramKey2="y",
        coord_treatment={"z": {"type": "const", "val": 15}},
        verbose=True,
    )
    assert not fig is None

    #
    # Parametric
    #
    fig = tab2.plot2D_interactive(
        paramKey1="x",
        paramKey2="y",
        verbose=True,
    )
    assert not fig is None


def test_rediscretize_AlyaTable():
    """
    Test rediscretizing Alya table.
    """
    tab = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )

    #
    # Wrong name
    #
    tab.rediscretizeAlongCoordinate(ck="www")

    #
    # Without ck being in tab.data
    #
    tab.rediscretizeAlongCoordinate(ck="y")

    assert tab.keys() == ["a", "b", "c"]
    assert np.abs(tab.coords["y"][1] - 0.1778279410038923) < 1e-7
    assert np.abs(tab.coords["y"][2] - 3.1622776601683795) < 1e-7
    assert np.abs(tab.data["a"][1, 1, 1] - 0.31092438101768494) < 1e-7
    assert np.abs(tab.data["a"][1, 2, 1] - 0.36134455) < 1e-7

    #########################
    # Real rediscretization #
    #########################
    #
    # Add y to data
    #
    iy = tab.coordKeys.index("y")
    ymatrix = np.empty(tab.shape)
    for i in range(np.product(tab.shape)):
        ind = i2inds(i, tab.shape)
        ymatrix[ind] = tab.coords["y"][ind[iy]]
    tab.__keys__.append("y")
    tab.data["y"] = ymatrix

    #
    # Modify y so the resampling has an effect
    #
    tab.data["y"] *= 2.0
    assert np.abs(tab.data["y"][1, 1, 1] - 0.3556558820077846) < 1e-7
    assert np.abs(tab.data["a"][1, 1, 1] - 0.31092438101768494) < 1e-7
    assert np.abs(tab.data["a"][1, 2, 1] - 0.36134455) < 1e-7

    #
    # Rediscretize
    #
    tab.rediscretizeAlongCoordinate("y")

    assert np.abs(tab.data["y"][1, 1, 1] - 0.1778279410038923) < 1e-7
    assert np.abs(tab.data["a"][1, 1, 1] - 0.28421215990476806) < 1e-7
    assert np.abs(tab.data["a"][1, 2, 1] - 0.33463232496671874) < 1e-7
    assert tab.keys() == ["a", "b", "c", "y"]
    assert tab.shape == tuple(shape)


def test_rediscretize_different_shape_AlyaTable():
    """
    Test rediscretizing Alya table wih changign the coordiante.
    """
    tab = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )

    #
    # Add y to data
    #
    iy = tab.coordKeys.index("y")
    ymatrix = np.empty(tab.shape)
    for i in range(np.product(tab.shape)):
        ind = i2inds(i, tab.shape)
        ymatrix[ind] = tab.coords["y"][ind[iy]]
    tab.__keys__.append("y")
    tab.data["y"] = ymatrix

    #
    # Rediscretize
    #
    tab.rediscretizeAlongCoordinate("y", np.logspace(-3, 4, 8))

    print(np.logspace(-3, 4, 8))

    assert np.abs(tab.data["y"][1, 1, 1] - 0.01) < 1e-7
    assert np.abs(tab.data["a"][1, 1, 1] - 0.26050421595573425) < 1e-7
    assert np.abs(tab.data["a"][1, 2, 1] - 0.2875427104306818) < 1e-7
    assert tab.keys() == ["a", "b", "c", "y"]
    assert tab.shape == tuple([4, 8, 6])
    assert tab.shape == tab.data["a"].shape


def test_rediscretize_1D_AlyaTable():
    """
    Test rediscretizing a 1 dimensional Alya table.
    """
    tab = AlyaTable()

    #
    # Add corrdinate
    #
    tab.addCoordinate("x", np.linspace(0.0, 1.0, 11))
    tab.addGridOfCoordinate("x")

    #
    # Add data
    #
    tab.addEmptyData("y")
    tab.data["y"] = tab.data["x"] ** 2.0
    assert np.abs(tab.data["y"][5] - 0.25) < 1e-7

    #
    # Shif coordinate
    #
    tab.data["x"] += 0.1

    #
    # Rediscretize taking tab.data["x"] to be the true coordinate
    #
    tab.rediscretizeAlongCoordinate("x")
    assert np.abs(tab.data["y"][0] - 0.0) < 1e-7
    assert np.abs(tab.data["y"][1] - 0.0) < 1e-7
    assert np.abs(tab.data["y"][5] - 0.16) < 1e-7
    assert np.abs(tab.data["y"][6] - 0.25) < 1e-7

    #
    # Rediscretize taking tab.data["x"] to be the true coordinate
    # And propose new discretiztion, changing the shape of the whole table
    #
    tab.rediscretizeAlongCoordinate("x", [0.0, 0.5, 0.7, 0.9, 1.0, 1.1])
    assert np.abs(tab.data["y"][0] - 0.0) < 1e-7
    assert np.abs(tab.data["y"][1] - 0.16) < 1e-7
    assert np.abs(tab.data["y"][2] - 0.36) < 1e-7
    assert np.abs(tab.data["y"][3] - 0.64) < 1e-7
    assert np.abs(tab.data["y"][4] - 0.81) < 1e-7
    assert np.abs(tab.data["y"][5] - 0.81) < 1e-7  # this was clipped during the shift


def test_coordinatesToData_AlyaTable():
    """
    Test putting the coordinates of the table in the data structure.
    """
    tab = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )

    assert not "x" in tab.data.keys()
    assert not "y" in tab.data.keys()
    assert not "z" in tab.data.keys()

    assert tab.nkeys == 3
    assert tab.keys() == ["a", "b", "c"]

    tab.coordinatesToData()

    assert "x" in tab.data.keys()
    assert "y" in tab.data.keys()
    assert "z" in tab.data.keys()

    assert tab.nkeys == 6
    assert tab.keys() == ["a", "b", "c", "x", "y", "z"]

    assert np.abs(tab.data["y"][0, 0, 0] - 0.0100000000000000) < 1e-7
    assert np.abs(tab.data["y"][0, 1, 0] - 0.1778279410038923) < 1e-7
    assert np.abs(tab.data["y"][0, 2, 0] - 3.1622776601683795) < 1e-7
    assert np.abs(tab.data["y"][1, 0, 0] - 0.0100000000000000) < 1e-7
    assert np.abs(tab.data["y"][1, 1, 0] - 0.1778279410038923) < 1e-7
    assert np.abs(tab.data["y"][1, 2, 0] - 3.1622776601683795) < 1e-7
    assert np.abs(tab.data["y"][0, 0, 1] - 0.0100000000000000) < 1e-7
    assert np.abs(tab.data["y"][0, 1, 1] - 0.1778279410038923) < 1e-7
    assert np.abs(tab.data["y"][0, 2, 1] - 3.1622776601683795) < 1e-7


def test_gradient_forward_AlyaTable():
    """
    Test differentiating Alya table along a coordinate.
    """
    tab = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )

    tab.partialDifferentiationAlongOneCoordinate("dummy", "a", "grada_dummy")
    tab.partialDifferentiationAlongOneCoordinate("x", "dummy", "graddummy_x")

    tab.partialDifferentiationAlongOneCoordinate("x", "a", "grada_x", scheme="forward")
    tab.partialDifferentiationAlongOneCoordinate("y", "a", "grada_y", scheme="forward")
    tab.partialDifferentiationAlongOneCoordinate("z", "a", "grada_z", scheme="forward")
    tab.partialDifferentiation(["z"], "a", "grada_full_z", scheme="forward")

    print(xcoord[1:3])
    print(ycoord[1:3])
    print(zcoord[1:3])

    assert np.abs(tab.data["a"][1, 1, 1] - 0.31092438) < 1e-7
    assert np.abs(tab.data["a"][2, 1, 1] - 0.56302524) < 1e-7
    assert np.abs(tab.data["a"][1, 2, 1] - 0.36134455) < 1e-7
    assert np.abs(tab.data["a"][1, 1, 2] - 0.31932774) < 1e-7
    assert np.abs(tab.data["grada_x"][1, 1, 1] - 0.75630254) < 1e-7
    assert np.abs(tab.data["grada_y"][1, 1, 1] - 0.016894292) < 1e-7
    assert np.abs(tab.data["grada_z"][1, 1, 1] - 0.0042016804) < 1e-7
    assert np.abs(tab.data["grada_full_z"][1, 1, 1] - 0.0042016804) < 1e-7

    #
    # Check if last derivative is same as last but one (Forward scheme)
    #
    assert np.abs(tab.data["grada_x"][-1, 1, 1] - tab.data["grada_x"][-2, 1, 1]) < 1e-7
    assert np.abs(tab.data["grada_y"][1, -1, 1] - tab.data["grada_y"][1, -2, 1]) < 1e-7
    assert np.abs(tab.data["grada_z"][1, 1, -1] - tab.data["grada_z"][1, 1, -2]) < 1e-7


def test_multiple_gradients_forward_AlyaTable():
    """
    Test differentiating Alya table along multiple coordinates.
    """
    tab = AlyaTable(
        dic=data,
        keys=["a", "b", "c"],
        coordKeys=["x", "y", "z"],
        coords={"x": xcoord, "y": ycoord, "z": zcoord},
        typ="single",
    )

    tab.addEmptyData("asq")
    tab.data["asq"] = np.power(tab.data["a"], 2)

    tab.partialDifferentiation(["x"], "asq", "gradasq_x", scheme="forward")
    tab.partialDifferentiation(["y"], "asq", "gradasq_y", scheme="forward")
    tab.partialDifferentiation(["x", "y"], "asq", "gradasq_xy", scheme="forward")
    tab.partialDifferentiation(["y", "x"], "asq", "gradasq_yx", scheme="forward")

    print(xcoord[1:3])
    print(ycoord[1:3])

    print(tab.data["asq"][1:3, 1:3, 1])
    print(tab.data["gradasq_x"][1, 1:3, 1])
    print(tab.data["gradasq_y"][1:3, 1, 1])
    print(tab.data["gradasq_xy"][1, 1, 1])
    print(tab.data["gradasq_yx"][1, 1, 1])

    assert np.abs(tab.data["asq"][1, 1, 1] - 0.09667397) < 1e-7
    assert np.abs(tab.data["asq"][2, 1, 1] - 0.3169974) < 1e-7
    assert np.abs(tab.data["asq"][1, 2, 1] - 0.13056988) < 1e-7
    assert np.abs(tab.data["gradasq_xy"][1, 1, 1] - 0.025554396) < 1e-7
    assert np.abs(tab.data["gradasq_yx"][1, 1, 1] - 0.025554413) < 1e-7
    assert np.all(np.abs(tab.data["gradasq_yx"] - tab.data["gradasq_xy"]) < 1e-5)


def test_multiple_gradients_of_analytical_function_AlyaTable():
    """
    Test differentiating Alya table along multiple coordinates where the data is a known function.
    """
    tab = AlyaTable(
        typ="single",
    )

    #
    # Build grid
    #
    tab.addCoordinate("x", np.linspace(0.0, np.pi, 50))
    tab.addCoordinate("y", np.linspace(0.0, np.pi, 50))
    tab.addCoordinate("z", np.linspace(0.0, np.pi, 50))

    tab.addGridOfAllCoordinates()

    #
    # Add functions
    #
    tab.addEmptyData("sins")
    tab.data["sins"] = (
        np.sin(tab.data["x"]) * np.sin(tab.data["y"]) * np.sin(tab.data["z"])
    )

    tab.addEmptyData("cossinsin")
    tab.data["cossinsin"] = (
        np.cos(tab.data["x"]) * np.sin(tab.data["y"]) * np.sin(tab.data["z"])
    )

    tab.addEmptyData("coscossin")
    tab.data["coscossin"] = (
        np.cos(tab.data["x"]) * np.cos(tab.data["y"]) * np.sin(tab.data["z"])
    )

    tab.addEmptyData("coss")
    tab.data["coss"] = (
        np.cos(tab.data["x"]) * np.cos(tab.data["y"]) * np.cos(tab.data["z"])
    )

    for scheme, tol in zip(["forward", "backward", "central"], [1e-1, 1e-1, 1e-2]):
        print("scheme and tol:", scheme, tol)

        if scheme == "central":
            tab.partialDifferentiation(["x"], "sins", "a_x")
        else:
            tab.partialDifferentiation(["x"], "sins", "a_x", scheme=scheme)
        tab.partialDifferentiation(["x", "y"], "sins", "a_xy", scheme=scheme)
        tab.partialDifferentiation(["x", "y", "z"], "sins", "a_xyz", scheme=scheme)

        assert np.max(np.abs(tab.data["a_x"] - tab.data["cossinsin"])) < tol
        assert np.max(np.abs(tab.data["a_xy"] - tab.data["coscossin"])) < tol
        assert np.max(np.abs(tab.data["a_xyz"] - tab.data["coss"])) < tol
        assert not np.max(np.abs(tab.data["a_x"] - tab.data["cossinsin"])) < tol * 1e-2
        assert not np.max(np.abs(tab.data["a_xy"] - tab.data["coscossin"])) < tol * 1e-2
        assert not np.max(np.abs(tab.data["a_xyz"] - tab.data["coss"])) < tol * 1e-2


def test_integrate_analytical_function_AlyaTable():
    """
    Test integrating Alya table along one coordinate where the data is a known function.
    """
    for typ, tols in zip(
        ["single", "double"], [[1e-1, 1e-1, 1e-16], [1e-1, 1e-1, 1e-16]]
    ):
        tab = AlyaTable(
            typ=typ,
        )

        #
        # Build grid
        #
        tab.addCoordinate("x", np.linspace(0.0, 1.0, 31))
        tab.addCoordinate("y", np.linspace(0.0, 1.0, 31))
        tab.addCoordinate("z", np.linspace(0.0, 1.0, 31))

        tab.addGridOfAllCoordinates()

        #
        # Add functions
        #
        tab.addEmptyData("xyz")
        tab.data["xyz"] = tab.data["x"] * tab.data["y"] * tab.data["z"]

        #
        # Try errors
        #
        assert (
            AlyaTable.partialIntegrationAlongOneCoordinate(
                tab, "dummy", "xyz", scheme="forward"
            )
            is None
        )
        assert (
            AlyaTable.partialIntegrationAlongOneCoordinate(
                tab, "x", "dummy", scheme="forward"
            )
            is None
        )
        assert (
            AlyaTable.partialIntegration(tab, ["dummy"], "xyz", scheme="forward")
            is None
        )
        assert (
            AlyaTable.partialIntegration(tab, ["x"], "dummy", scheme="forward") is None
        )

        #
        # Try real integration
        #
        for scheme, tol in zip(["forward", "backward", "trapezoidal"], tols):
            print(typ, scheme, tol)

            inttab = AlyaTable.partialIntegrationAlongOneCoordinate(
                tab, "x", "xyz", scheme=scheme
            )
            inttab2 = AlyaTable.partialIntegration(tab, ["x"], "xyz", scheme=scheme)

            assert np.abs(np.max(inttab.data["xyz"]) - 0.5) < tol
            assert np.abs(np.max(inttab2.data["xyz"]) - 0.5) < tol
            if scheme != "trapezoidal":
                assert not np.abs(np.max(inttab.data["xyz"]) - 0.5) < tol * 1e-3
                assert not np.abs(np.max(inttab2.data["xyz"]) - 0.5) < tol * 1e-3


def test_integrate_analytical_function_along_multiple_coordinates_AlyaTable():
    """
    Test integrating Alya table along multiple coordinates where the data is a known function.
    """
    tab = AlyaTable(
        typ="single",
    )

    #
    # Build grid
    #
    tab.addCoordinate("x", np.linspace(0.0, 1.0, 31))
    tab.addCoordinate("y", np.linspace(0.0, 1.0, 36))
    tab.addCoordinate("z", np.linspace(0.0, 1.0, 41))

    tab.addGridOfAllCoordinates()

    #
    # Add functions
    #
    tab.addEmptyData("xyz")
    tab.data["xyz"] = tab.data["x"] * tab.data["y"] * tab.data["z"]

    #
    # Try real integration
    #
    for scheme, tol in zip(["forward", "backward", "trapezoidal"], [1e-1, 1e-1, 1e-16]):
        print(scheme, tol)

        if scheme == "trapezoidal":
            inttab1 = AlyaTable.partialIntegration(tab, ["x", "y", "z"], "xyz")
        else:
            inttab1 = AlyaTable.partialIntegration(
                tab, ["x", "y", "z"], "xyz", scheme=scheme
            )
        inttab2 = AlyaTable.partialIntegration(
            tab, ["y", "z", "x"], "xyz", scheme=scheme
        )
        inttab3 = AlyaTable.partialIntegration(
            tab, ["z", "x", "y"], "xyz", scheme=scheme
        )

        print(
            "xyz, yzx, zxy",
            inttab1.data["xyz"][0],
            inttab2.data["xyz"][0],
            inttab3.data["xyz"][0],
        )

        assert np.abs(inttab1.data["xyz"][0] - 0.125) < tol
        assert np.abs(inttab2.data["xyz"][0] - 0.125) < tol
        assert np.abs(inttab3.data["xyz"][0] - 0.125) < tol

        if scheme != "trapezoidal":
            assert not np.abs(np.max(inttab1.data["xyz"]) - 0.125) < tol * 1e-3
            assert not np.abs(np.max(inttab2.data["xyz"]) - 0.125) < tol * 1e-3
            assert not np.abs(np.max(inttab3.data["xyz"]) - 0.125) < tol * 1e-3
