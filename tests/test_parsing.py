#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
from .context import alyatab, getTestPath, getTestTmpPath
from alyatab.parsing import stripCharactersFromString, searchExpression

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_stripChars():
    """
    Test stripping characters from string.
    """
    assert stripCharactersFromString("abc", ["d"]) == "abc"
    assert stripCharactersFromString("dadbdcd", ["d"]) == "abc"
    assert (
        stripCharactersFromString("#This was commented", ["#"]) == "This was commented"
    )
    assert stripCharactersFromString("//Also/ commented/", ["//"]) == "Also/ commented/"


def test_searchExpression():
    """
    Test looking for expression in text file.
    """

    with open(os.path.join(test_path, "data", "exampleTextForParsing.txt"), "r") as f:
        #
        # Search for something that is not in the file.
        #
        line, nline = searchExpression(
            f, "notintext", position=0, stripNonAlphaNum=True, returnToTop=True
        )
        assert line is None

        #
        # Search for first occurance of an option:
        #
        line, nline = searchExpression(
            f, "OPT1", position=0, stripNonAlphaNum=True, returnToTop=True
        )
        assert line[0] == "OPT1"
        assert line[1] == "DEF"

        #
        # Search for second occurance of an option:
        #
        searchExpression(f, "OPT1", position=0, stripNonAlphaNum=True, returnToTop=True)
        line, nline = searchExpression(
            f, "OPT1", position=0, stripNonAlphaNum=True, returnToTop=False
        )
        assert line[0] == "OPT1"
        assert line[1] == "GHI"

        #
        # Search only inside given section
        # OPT2 is only present after section ends
        #
        searchExpression(f, "SEC1", position=0, stripNonAlphaNum=True, returnToTop=True)
        line, nline = searchExpression(
            f,
            "OPT2",
            position=0,
            stripNonAlphaNum=True,
            returnToTop=False,
            stripCharacterList=["_"],
            terminationKeyword="ENDSEC1",
        )
        assert line is None
        #
        # Find First OPT1 in SEC1
        #
        searchExpression(f, "SEC1", position=0, stripNonAlphaNum=True, returnToTop=True)
        line, nline = searchExpression(
            f,
            "OPT1",
            position=0,
            stripNonAlphaNum=True,
            returnToTop=False,
            stripCharacterList=["_"],
            terminationKeyword="ENDSEC1",
        )
        assert line[0] == "OPT1"
        assert line[1] == "GHI"

        #
        # Search commented part too:
        #
        line, nline = searchExpression(
            f,
            "OPT1",
            position=0,
            stripNonAlphaNum=True,
            stripComment=["#"],
            returnToTop=True,
        )
        assert line[0] == "OPT1"
        assert line[1] == "ABC"
        assert line[2] == "COMMENTED"

        #
        # Search case sensitive by default:
        #
        line, nline = searchExpression(
            f, "opt1", position=0, stripNonAlphaNum=True, returnToTop=True
        )
        assert line[0] == "opt1"
        assert line[1] == "EFG"
        #
        # Search case insensitive
        #
        line, nline = searchExpression(
            f,
            "opt1",
            position=0,
            stripNonAlphaNum=True,
            returnToTop=True,
            caseSensitive=False,
        )
        assert line[0] == "OPT1"
        assert line[1] == "DEF"

        #
        # Search only inside given section but case insensitive
        # OPT2 is only present after section ends
        #
        searchExpression(
            f,
            "SEC1",
            position=0,
            stripNonAlphaNum=True,
            returnToTop=True,
            caseSensitive=False,
        )
        line, nline = searchExpression(
            f,
            "OPT2",
            position=0,
            stripNonAlphaNum=True,
            returnToTop=False,
            stripCharacterList=["_"],
            terminationKeyword="ENDSEC1",
            caseSensitive=False,
        )
        assert line is None
        #
        # Find First OPT1 in SEC1
        #
        searchExpression(
            f,
            "SEC1",
            position=0,
            stripNonAlphaNum=True,
            returnToTop=True,
            caseSensitive=False,
        )
        line, nline = searchExpression(
            f,
            "OPT1",
            position=0,
            stripNonAlphaNum=True,
            returnToTop=False,
            stripCharacterList=["_"],
            terminationKeyword="ENDSEC1",
            caseSensitive=False,
        )
        assert line[0].upper() == "OPT1"
        assert line[1].upper() == "XYZ"

        #
        # Search second column:
        #
        line, nline = searchExpression(
            f, "GHI", position=0, stripNonAlphaNum=True, returnToTop=True
        )
        assert line is None

        line, nline = searchExpression(
            f, "GHI", position=1, stripNonAlphaNum=True, returnToTop=True
        )
        assert line[0] == "OPT1"
        assert line[1] == "GHI"
        assert nline == 19
