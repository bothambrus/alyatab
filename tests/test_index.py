#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import alyatab
from alyatab.index import i2inds, i2inds_vec, inds2i


#
# Testing indexing methods
#
def test_i2inds():
    """
    Test converting flattened index into array of indexes based on shape.
    """
    assert i2inds(2, [4]) == (2,)
    assert i2inds(7, [4, 4]) == (1, 3)
    assert i2inds(18, [4, 4, 4]) == (1, 0, 2)


def test_inds2i():
    """
    Test converting array of indexes to flattened index based on shape.
    """
    assert inds2i([1, 3], [4, 4]) == 7
    assert inds2i([1, 0, 2], [4, 4, 4]) == 18


def test_i2inds_vec():
    """
    Test converting flattened index into array of indexes based on shape.
    """
    assert i2inds_vec(np.array([2]), [4])[0, :] == np.array([2])
    assert np.all(i2inds_vec(np.array([7]), [4, 4])[0, :] == np.array([1, 3]))
    assert np.all(i2inds_vec(np.array([18]), [4, 4, 4])[0, :] == np.array([1, 0, 2]))

    inds = i2inds_vec(np.array([14, 15, 16, 17, 18]), [4, 4, 4])
    print(inds)
    assert np.all(
        inds
        == np.array(
            [
                [0, 3, 2],
                [0, 3, 3],
                [1, 0, 0],
                [1, 0, 1],
                [1, 0, 2],
            ]
        )
    )
