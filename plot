#!/usr/bin/env python
#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import sys


def main(argv):
    """
    This is the plotting script of AlyaTab.
    It open a given table as an interative plot.

    Execution:
    ./plot [OPTIONS]


    OPTIONS:
        -h  --help:
                Print this message.

        -i  --input:
                Path to table file.

        -x  --xkey:
                control variable of horizontal axis.

        -y  --ykey:
                control variable of vertical axis.

        -o  --output:
                Output file name. By default: tmp.html

    Example:
        ./plot -x Z -y C -i tests/data/example_flamelet_table/example_flamelet_table.dat -o Z_C_plots.html
    """
    #
    # Import
    #
    import getopt
    import os

    from alyatab.table import AlyaTable
    from alyatab.interactive_plotting import getPlotly

    #
    # Process options and arguments
    #
    short_arguments = "hi:x:y:o:"
    long_arguments = [
        "help",
        "input=",
        "xkey=",
        "ykey=",
        "output=",
    ]
    try:
        opts, args = getopt.gnu_getopt(argv, short_arguments, long_arguments)
    except getopt.GetoptError:
        help(main)

    isHelp = False
    tablePath = None
    xKey = None
    yKey = None
    filename = "tmp.html"

    #
    # Process options
    #
    for opt, arg in opts:
        if opt in ["-h", "--help"]:
            isHelp = True
        elif opt in ["-i", "--input"]:
            tablePath = arg
            if not os.path.isfile(tablePath):
                print("Input (-i/--input) has to be a file.")
                isHelp = True
        elif opt in ["-x", "--xkey"]:
            xKey = arg
        elif opt in ["-y", "--ykey"]:
            yKey = arg
        elif opt in ["-o", "--output"]:
            filename = arg
            if os.path.isfile(filename):
                print("Overwriting {}.".format(filename))

    if len(opts) == 0:
        isHelp = True

    #
    # Print help
    #
    if isHelp:
        help(main)
        sys.exit()

    #
    # Initialize Alya table
    #
    tab = AlyaTable()

    #
    # Read table
    #
    tab.readText(tablePath)

    #
    # Create interactive 2D plot
    #
    fig = tab.plot2D_interactive(xKey, yKey, verbose=True)

    #
    # Show figure
    #
    py = getPlotly()
    py.offline.plot(fig, filename=filename, auto_open=True)


if __name__ == "__main__":
    main(sys.argv[1:])
