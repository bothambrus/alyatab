#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import sys
import numpy as np

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from alyatab.table import AlyaTable
from alyatab.interactive_plotting import getPlotly

#
# Initialize Alya table
#
tab = AlyaTable()

#
# Read and Alya table from test data
#
tab_path = os.path.join(
    os.path.dirname(__file__),
    "..",
    "tests",
    "data",
    "example_flamelet_table",
    "example_flamelet_table.dat",
)


#
# Read table
#
tab.readText(tab_path)

fig = tab.plot2D_interactive("Z", "C", verbose=True)

py = getPlotly()
# py.offline.plot(fig, filename = 'filename.html', auto_open=False)
py.offline.plot(fig)
