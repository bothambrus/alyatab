#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import sys
import numpy as np

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from alyatab.table import AlyaTable
from alyatab.interactive_plotting import getPlotly


tab = AlyaTable(
    typ="double",
)

#
# Build grid
#
tab.addCoordinate("x", np.linspace(0.0, 2.0 * np.pi, 51))
tab.addCoordinate("y", np.linspace(0.0, 2.0 * np.pi, 51))
tab.addCoordinate("z", np.linspace(0.0, 2.0 * np.pi, 51))

tab.addGridOfAllCoordinates()

#
# Add function
#
tab.addEmptyData("sins")
tab.data["sins"] = np.sin(tab.data["x"]) * np.sin(tab.data["y"]) * np.sin(tab.data["z"])

#
# Differentiate
#
scheme = "central"
tab.partialDifferentiation(["x"], "sins", "a_x", scheme=scheme)
tab.partialDifferentiation(["x", "y"], "sins", "a_xy", scheme=scheme)
tab.partialDifferentiation(["x", "y", "z"], "sins", "a_xyz", scheme=scheme)


#
# Plot
#
fig = tab.plot2D_interactive(
    "x", "y", coord_treatment={"z": {"type": "const", "val": 0.5 * np.pi}}, verbose=True
)
py = getPlotly()
py.offline.plot(fig)

#
# Integrate along x
#
intscheme = "trapezoidal"
# intscheme="forward"
inttab = AlyaTable.partialIntegrationAlongOneCoordinate(
    tab, "x", "sins", scheme=intscheme
)


#
# Plot
#
fig = inttab.plot2D_interactive("y", "z", verbose=True)
py = getPlotly()
py.offline.plot(fig, filename="tmp.html", auto_open=True)
