#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import sys
import numpy as np

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from alyatab.table import AlyaTable

#
# Initialize Alya table
#
tab = AlyaTable()

#
# Read and Alya table from test data
#
tab_path = os.path.join(
    os.path.dirname(__file__),
    "..",
    "tests",
    "data",
    "example_flamelet_table",
    "example_flamelet_table.dat",
)

#
# Read table
#
tab.readText(tab_path)

#
# Print some key information:
#
print("Table data shape:  {}".format(tab.shape))
print("Table corrdinates: {}".format(tab.coordKeys))
print("Table variables:   {}".format(tab.keys()))

#
# Tnterpolate accoding to an array of control variables:
# The last dimension means the different control variables
# The rest of the shape is arbitrary
#
valarr = np.zeros([3, 3, 2])
valarr[0, 0, :] = [0.05, 0.75]
valarr[1, 0, :] = [0.0, 0.0]
valarr[2, 0, :] = [1.0, 1.0]
valarr[0, 1, :] = [0.052, 0.78]
valarr[1, 1, :] = [0.02, 0.02]
valarr[2, 1, :] = [0.98, 0.98]
valarr[0, 2, :] = [-0.5, 0.0]
valarr[1, 2, :] = [0.0, -0.5]
valarr[2, 2, :] = [-0.5, -0.5]


data = tab.interpolateArray(valarr, key_list=["T", "omegaYc"], verbose=True)


print("Given Z coordinates: \n{}".format(valarr[:, :, 0]))
print("Given C coordinates: \n{}".format(valarr[:, :, 1]))
print("Temperature at given coordinates: \n{}".format(data["T"]))
print("Source term at given coordinates: \n{}".format(data["omegaYc"]))

print()
print()
print()
val = [0.05, 1e-2]
data2 = tab.interpolate(
    val, key_list=["C", "Yc", "T", "omegaYc"], unscaledVar={"C": "Yc"}
)


print("Individual interpolation along unscaled data")
print("Given Z coordinate: \n{}".format(val[0]))
print("Given Yc coordinate: \n{}".format(val[1]))
print("C at given coordinate: \n{}".format(data2["C"]))
print("Yc at given coordinate: \n{}".format(data2["Yc"]))
print("Temperature at given coordinate: \n{}".format(data2["T"]))
print("Source term at given coordinate: \n{}".format(data2["omegaYc"]))
