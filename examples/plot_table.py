#################################################################################
#   AlyaTab                                                                     #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import sys
import numpy as np

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from alyatab.table import AlyaTable
from alyatab.plotting import setPltBackend

#
# Initialize Alya table
#
tab = AlyaTable()

#
# Read and Alya table from test data
#
tab_path = os.path.join(
    os.path.dirname(__file__),
    "..",
    "tests",
    "data",
    "example_flamelet_table",
    "example_flamelet_table.dat",
)

#
# Read table
#
tab.readText(tab_path)

#
# Plot 1D slices of table
#
plt = setPltBackend()
fig, ax = tab.plot1D(
    paramKey1="C",
    xKey="Yc",
    yKey="omegaYc",
    coord_treatment={"Z": {"type": "const", "val": 0.05}},
    verbose=True,
)


fig, ax = tab.plot1D(
    paramKey1="C",
    xKey="Yk_CO2",
    yKey="omegak_CO2",
    coord_treatment={"Z": {"type": "all"}},
    verbose=True,
)


fig, ax = tab.plot1D(
    paramKey1="Z",
    xKey="Z",
    yKey="omegaYc",
    coord_treatment={"C": {"type": "const", "val": 0.8}},
    verbose=True,
)


fig, ax = tab.plot1D(
    paramKey1="Z",
    xKey="Z",
    yKey="omegaYc",
    coord_treatment={"C": {"type": "constUnscaled", "val": 0.018, "unscaledKey": "Yc"}},
    verbose=True,
)

fig, ax = tab.plot1D(
    paramKey1="Z",
    xKey="Z",
    yKey="omegaYc",
    coord_treatment={"C": {"type": "constUnscaled", "val": 0.018, "unscaledKey": "Yc"}},
    allowExtrapolate=False,
    verbose=True,
)


fig, ax = tab.plot2D(
    paramKey1="Z",
    paramKey2="C",
    xKey="Z",
    yKey="Yk_CO2",
    zKey="omegak_CO2",
    verbose=True,
)

fig, ax = tab.plot2D(
    paramKey1="Z",
    paramKey2="C",
    xKey="Z",
    yKey="Yk_CO2",
    zKey="omegak_CO2",
    plot_type="contour",
    verbose=True,
    cmin=-20,
    cmax=150,
    n_isoline=[1, 40, 80, 120, 150],
)

fig, ax = tab.plot2D(
    paramKey1="Z",
    paramKey2="C",
    xKey="Z",
    yKey="Yk_CO2",
    zKey="omegak_CO2",
    plot_type="plot_surface",
    verbose=True,
)


plt.show()
